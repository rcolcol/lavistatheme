<?php
/**
 * Admin Page
 *
 * @package WordPress
 * @version 1.0.2
 */

get_header();?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<?php if (is_user_logged_in()) : 
		function human_filesize($bytes, $decimals = 2) {
			$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
			$factor = floor((strlen($bytes) - 1) / 3);
			return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
		}

		function timeBetween($start, $end=-1){
			if($end < 0) $end = time();
			$SECOND = 1;
			$MINUTE = 60 * $SECOND;
			$HOUR = 60 * $MINUTE;
			$DAY = 24 * $HOUR;
			$WEEK = 7 * $DAY;
			$MONTH = 30 * $DAY;
			$YEAR = 365 * $DAY;
			$increments = [
				[$SECOND, 'second'],
				[$MINUTE, 'minute'],
				[$HOUR, 'hour'],
				[$DAY, 'day'],
				[$WEEK, 'week'],
				[$MONTH, 'month'],
				[$YEAR, 'year']
			];

			$diff = $end - $start;
			$plural = '';
			$units = ceil($diff/$increments[count($increments)-1][0]);
			$unit = $increments[count($increments)-1][1];
			for($i = 1; $i < count($increments); $i++){
				if($increments[$i-1][0] <= $diff && $diff < $increments[$i][0]){
					$units = ceil($diff/$increments[$i-1][0]);
					$unit = $increments[$i-1][1];
					break;
				}
			}

			if($units > 1) $plural = 's';
			return sprintf("%d %s%s", $units, $unit, $plural);
		} ?>
		<div class="main_content_wrapper">
			<div id="sidebar_left" class="sidebars">
				<?php
					wp_nav_menu( array(
						'menu'		     => 'News & updates Menu Sidebar',
						'menu_id'        => 'primary',
						'container'      => false,
						'depth'          => 2,
						'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
						'walker'         => new Bootstrap_NavWalker(),
						'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
					) ); ?>
            	<h5>Licensee</h5>
				<?php
					wp_nav_menu( array(
						'menu'		     => 'Licensee Menu Sidebar',
						'menu_id'        => 'primary',
						'container'      => false,
						'depth'          => 2,
						'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
						'walker'         => new Bootstrap_NavWalker(),
						'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
					) ); ?>
				<h5>Client Advice</h5>
				<?php
					wp_nav_menu( array(
						'menu'		     => 'Client Advice Menu Sidebar',
						'menu_id'        => 'primary',
						'container'      => false,
						'depth'          => 2,
						'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
						'walker'         => new Bootstrap_NavWalker(),
						'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
					) ); ?>
					<h5>Research</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Research Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) ); ?>
					<h5>Practice Management</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Practice Management Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) ); 
				?>

			</div>
			<div class="main_content">
				<?php $img_url = (has_post_thumbnail()) ? get_the_post_thumbnail_url(get_the_ID(), 'full') : false; ?>
				<div class="s_hero" <?php echo $img_url ? 'style="background-image:url($img_url); background-repeat: no-repeat;background-size: cover;"' : '' ?>>
					<?php $content = get_the_content(); if($content): ?>
						<div class="container">
							<div class="hero_content"><?php echo apply_filters('the_content', $content); ?></div>
						</div>
					<?php endif; ?>
				</div>

				<div class="container" style="margin-top:30px; margin-bottom:40px;">
					<div class="row">
						<?php $section_title = get_field('core_section_title'); 
						if ($section_title) :?>
						<div class="col-md-12">
							<h2><?php echo get_field('core_section_title'); ?></h2>			
						</div>
						<?php endif; ?>

						<div class="col-md-12">
							<div class="card-deck-wrapper">
								<?php //$loop_counter = 0;
								// check if the flexible content field has rows of data
								if( have_rows('add_new_block') ):
									// loop through the rows of data
									while ( have_rows('add_new_block') ) : the_row();
										// if( $loop_counter % 2 == 0) : echo '</div><div class="card-deck">'; endif;
										// if( $loop_counter % 2 == 0) : echo '<div class="card_wrapper">';
											// $loop_counter++;

											if( get_row_layout() == 'free_text' ): ?>
												<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck full_width"><div class="card bg-light mb-3">'; 
												elseif( get_sub_field('block_width') == 'half' ): echo '<div class="card-deck half_width"><div class="card bg-light mb-3">'; 
												else: echo '<div class="card-deck default_width"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
													<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
													<div class="card-body">
														<?php the_sub_field('block_content'); ?>
														<?php if( get_sub_field('page_links_to') != '' ): 
															echo '<a class="btn btn-primary" href="';
															the_sub_field('page_links_to');
															echo '" role="button">Read more</a>';
														endif; ?>
													</div>
												</div>
											</div>

											<?php elseif( get_row_layout() == 'schedule_a_service' ): ?>
												<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">'; 
												else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
													<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
													<div class="card-body">
														<?php echo do_shortcode( '[contact-form-7 id="516" title="Schedule a service"]' ); ?>
													</div>
												</div>
											</div>

											<?php elseif( get_row_layout() == 'event_calendar' ): ?>
												<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">'; 
												else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
													<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
													<div class="card-body">
														<?php if( have_rows('event_details') ): ?>
															<?php while( have_rows('event_details') ): the_row(); ?>
																<p class="card-text"><strong><?php the_sub_field('event_date'); ?></strong> - <?php the_sub_field('event_name'); ?><br><?php the_sub_field('event_description'); ?></p>
															<?php endwhile; ?>
														<?php endif; ?>
													</div>
												</div>
											</div>

											<?php elseif( get_row_layout() == 'advice_documents' ): ?>
												<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">';  
												else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
													<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
													<div class="card-body">
														<?php if( have_rows('documents') ): ?>
															<?php while( have_rows('documents') ): the_row(); ?>
																<?php $attachment_id = get_sub_field('the_document');
																	$filesize = filesize( get_attached_file( $attachment_id ) );
																	$last_edited = filectime( get_attached_file( $attachment_id ) ); ?>
																<p class="card-text"><a href="<?php echo wp_get_attachment_url( $attachment_id ); ?>" target="_blank"><?php the_sub_field('document_title'); ?></a> - <?php echo human_filesize($filesize); ?> - Last edited <?php echo timeBetween($last_edited); ?></p>
															<?php endwhile; ?>
														<?php endif; ?>
													</div>
												</div>
											</div>

											<?php elseif( get_row_layout() == 'add_standard_paragraph' ):?>
												<!-- The block of content -->
												<div class="card-deck">
													<?php the_sub_field('block_content'); ?>
												</div>

											<?php else: ?>
												<div>
													<h4>No content added.</h4>
												</div>
											<?php endif; ?>
										<!-- </div> -->
										<?php // endif; 
										// $loop_counter++; ?>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<!-- ADD ON SECTION -->
					<div class="row" style="margin-top:20px; display: none;">
						<div class="col-md-12">
							<?php $addon_title = get_field('add_on_title'); if ($addon_title) : ?>
								<h2><?php echo get_field('add_on_title'); ?></h2>	
							<?php endif; ?>		
						</div>
					</div>
					
					<div class="row" style="display: none;">
						<div class="col-md-12">
							<div class="card-deck">
								<?php 
								$user_details = "user_".get_current_user_id();
								$additional_packages = get_field('additional_packages', $user_details);
								$loop_counter = 0;
								// check if the flexible content field has rows of data
								if( have_rows('add_new_block_copy') ):
								// loop through the rows of data
									while ( have_rows('add_new_block_copy') ) : the_row();
										$check_the_title = get_sub_field('block_title');
										/*if( $loop_counter % 2 == 0) : echo '</div><div class="card-deck">'; endif;*/
										$loop_counter++;
										if( get_row_layout() == 'free_text' ): ?>
											<div class="card bg-light mb-3" >
												<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
												<div class="card-body">
													<?php if(in_array($check_the_title,$additional_packages)) : the_sub_field('block_content');  else : echo the_sub_field('locked_content');; endif; ?>
												</div>
											</div>

										<?php elseif( get_row_layout() == 'schedule_a_service' ): ?>
											<div class="card bg-light mb-3" >
												<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
												<div class="card-body">
													<?php echo do_shortcode( '[contact-form-7 id="516" title="Schedule a service"]' ); ?>
												</div>
											</div>

										<?php elseif( get_row_layout() == 'event_calendar' ): ?>
											<div class="card bg-light mb-3" >
												<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
												<div class="card-body">
													<?php if( have_rows('event_details') ): ?>
														<?php while( have_rows('event_details') ): the_row(); ?>
															<p class="card-text"><strong><?php the_sub_field('event_date'); ?></strong> - <?php the_sub_field('event_name'); ?><br><?php the_sub_field('event_description'); ?></p>
														<?php endwhile; ?>
													<?php endif; ?>
												</div>
											</div>

										<?php elseif( get_row_layout() == 'add_standard_paragraph' ): ?>
											</div>
											<!-- The block of content -->
											<?php the_sub_field('block_content'); ?>
											<div class="card-deck">

										<?php elseif( get_row_layout() == 'advice_documents' ): ?>
											<div class="card bg-light mb-3" >
												<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
												<div class="card-body">
												<?php if(in_array($check_the_title,$additional_packages)) : ?>
													<?php if( have_rows('documents') ): ?>
														<?php while( have_rows('documents') ): the_row(); ?>
															<?php $attachment_id = get_sub_field('the_document');
																$filesize = filesize( get_attached_file( $attachment_id ) );
																$last_edited = filectime( get_attached_file( $attachment_id ) ); ?>
															<p class="card-text"><a href="<?php echo wp_get_attachment_url( $attachment_id ); ?>" target="_blank"><?php the_sub_field('document_title'); ?></a> - <?php echo human_filesize($filesize); ?> - Last edited <?php echo timeBetween($last_edited); ?></p>
														<?php endwhile; ?>
													<?php endif; ?>
												<?php else : the_sub_field('locked_content'); endif; ?>
												</div>
											</div>
										<?php endif;
									endwhile; endif; ?>
								</div>
								<div class="card-deck" style="display: none;">
								<div class="card bg-light mb-3" >
									<div class="card-header font-weight-bold">Request a quote</div>
									<div class="card-body">
										<?php echo do_shortcode( '[contact-form-7 id="658" title="Request a quote"]' ); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="sidebar_right" class="sidebars">
		        
				<!-- Good to know  -->
				<?php $menuname = 'Latest Compliance Updates';
				if (wp_get_nav_menu_items($menuname)) : ?>
					<div class="widget menu_widget">
						<h2 class="widgettitle"><?php echo $menuname ?></h2>
						<?php
							wp_nav_menu( array(
								'menu'					 => $menuname,
								'menu_id'        => 'gtk_menu',
								'container'      => false,
								'depth'          => 2,
								'menu_class'     => 'navbar-nav ml-auto',
								'walker'         => new Bootstrap_NavWalker(),
								'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
							) );
						?>
					</div>
					<?php
				endif; ?>
				
				<?php 
				get_template_part( 'template-parts/content/event', 'widget', array('mobile' => false) );

				dynamic_sidebar('Sidebar Widget Area 2'); ?>

			</div>
		</div>
		<?php else : ?>
			<div class="container">	
				<div class="row">
					<div class="col-md-12 about-text" style="margin-bottom:40px;">
						<?php echo wpdm_login_form(); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
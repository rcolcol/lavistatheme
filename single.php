<?php get_header();?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

	<div class="container <?php echo is_user_logged_in() ? 'admin_sub_page' : ''; ?>">	
		<div class="row">
			<div class="col-md-12 about-text" style="margin-bottom:40px;">
				<?php 
				if (is_user_logged_in()) { 
					$categories = get_the_category();
					$category_id = $categories[0]->cat_ID;
					if (get_post_type() === 'event') :
						get_template_part( 'template-parts/content/event', 'single' );
					elseif($category_id==170 && !empty(get_field('new_lavista_post_permalink', get_the_ID()))): ?>
						<script type="text/javascript">location.href = '<?php echo get_field('new_lavista_post_permalink', get_the_ID())?>';</script>
					<?php else : 
						?>
						<h1><?php get_the_title() ?></h1>
						<?php the_content(); 
					endif;
				} else { 
					echo wpdm_login_form();
				} ?>
			</div>
		</div>
	</div>

	<?php
	endwhile; 
endif; ?>

<?php get_footer(); ?>
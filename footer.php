<?php
/**
 * Footer
 *
 * @package WordPress
 * @version 1.0.3
 */
?>

	<div class="cta-banner">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<h4>Schedule a call back so we can answer your questions</h4>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<button class="btn btn-outline-light btn-lg float-sm-right" type="button" data-toggle="modal" data-target="#schedulecallback">Schedule a call</button>
				</div>
			</div>
		</div>
	</div>
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<p><a href="/"><img src="<?php bloginfo('template_directory'); ?>/assets/images/lavista-logo-white.png" alt="LaVista logo"></a></p>
					<p><strong>Location</strong><br>Level 13, Corporate Centre One<br>2 Corporate Court <br>Bundall QLD 4217 </p>
					<p><strong>Phone:</strong><br>1300 557 598</p>
					<p><strong>Email:</strong><br>enquiries@lavista.com.au</p>
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<?php echo do_shortcode( '[contact-form-7 id="244" title="Contact form 1"]' ); ?>
				</div>
			</div>
			<div class="copyright_text">
				<span class="inline-desktop">COPYRIGHT © <?php echo date('Y'); ?> <a href="">LaVista Licensee Solutions Pty Ltd</a></span> <span class="hide-on-mobile">|</span> <span class="inline-desktop"><a href="#" data-toggle="modal" data-target="#privacypolicy">Privacy Policy</a></span> <span class="hide-on-mobile">|</span> <span class="inline-desktop"><a href="#" data-toggle="modal" data-target="#termsofuse">Website terms of use</a>
				</span>
			</div>
		</div>
	</section>
	<!-- Modal -->
	<div class="modal fade" id="termsofuse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			<?php echo get_field('terms_of_use',479); ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	<div class="modal fade" id="privacypolicy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			<?php echo get_field('privacy_policy',3); ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	<div class="modal fade" id="schedulecallback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4>Schedule a call back</h4>
		  </div>
		  <div class="modal-body">
			<?php echo do_shortcode( '[contact-form-7 id="413" title="Contact form 1"]' ); ?>
		  </div>
		</div>
	  </div>
	</div>
	<div id="backto_top" class="backto_top"><i class="fas fa-angle-up"></i></div>
<!-- 	<script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- <script type="text/javascript" src="<?php //bloginfo('template_directory'); ?>/assets/js/slick.min.js"></script> -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
	    jQuery(document).ready(function($) {
	      $('.client-list').slick({
	        dots: false,
	        infinite: true,
	        slidesToShow: 5,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 2000,
	        responsive: [
	          {
	            breakpoint: 992,
	            settings: {
	              slidesToShow: 4,
	              slidesToScroll: 1
	            }
	          },
	          {
	            breakpoint: 768,
	            settings: {
	              slidesToShow: 3,
	              slidesToScroll: 1
	            }
	          },
	          {
	            breakpoint: 480,
	            settings: {
	              slidesToShow: 2,
	              slidesToScroll: 1
	            }
	          }
	        ]
	      });

				$('#client-reviews').slick({
						dots: false,
						infinite: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: 5000,
					});
	    });
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-130126944-1', 'auto');
	  ga('send', 'pageview');
	</script>
	<?php wp_footer(); ?>

	<style>#cookie-law-div{z-index:10000000;position:fixed;bottom:1%;padding:1em;max-width:100%;width:100%;border-radius:10px;background:#000;border:1px solid rgba(0,0,0,.15);font-size:15px;box-shadow:rgba(23,43,99,.4) 0 7px 28px}#cookie-law-div a{font-size:15px;text-decoration:none;border-bottom:1px solid rgba(0,0,0,.5);color: #007bff}#cookie-law-div a:hover{opacity:.7}#cookie-law-div p{margin:0;color:#fff;padding-right:50px;}#cookie-law-div button{position:absolute;right:.5em;align-self:center;line-height:1;color:#fff;background-color:#000;border: 1px solid #fff;opacity: 1;font-size:12px;cursor:pointer;padding:0.7em;margin-right:1em;top:0;margin-top:0.9em;}#cookie-law-div button:hover{opacity:.8}</style>
	<script>cookieLaw = { dId: "cookie-law-div", bId: "cookie-law-button", iId: "lavista-cookie-law-item", show: function (e) { if (localStorage.getItem(cookieLaw.iId)) return !1; var o = document.createElement("div"), i = document.createElement("p"), t = document.createElement("button"); i.innerHTML = e.msg, t.id = cookieLaw.bId, t.innerHTML = e.ok, o.id = cookieLaw.dId, o.appendChild(t), o.appendChild(i), document.body.insertBefore(o, document.body.lastChild), t.addEventListener("click", cookieLaw.hide, !1) }, hide: function () { document.getElementById(cookieLaw.dId).outerHTML = "", localStorage.setItem(cookieLaw.iId, "1") } }, cookieLaw.show({ msg: "By using this website, you agree to our use of cookies. We use cookies to provide you with a great experience and to help our website run effectively. To know more, check our <a href='https://lavista.com.au/privacy-policy/' target='_blank'>Cookie Policy</a>", ok: "OK" });</script>
</body>
</html>
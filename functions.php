<?php

require_once(__DIR__ . '/inc/md-main.php');

require get_template_directory() . '/inc/bootstrap-navwalker.php';

add_theme_support( 'post-thumbnails' );
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
  function wpt_setup() {  
      register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
} endif;


// Changing excerpt more
function new_excerpt_more($more) {
return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}

function acf_load_additional_packages_choices( $field ) {
    // reset choices
    $field['choices'] = array();
    if( have_rows('add_new_block_copy', 463) ):
		while ( have_rows('add_new_block_copy', 463) ) : the_row();
			$choice = get_sub_field('block_title');
			$field['choices'][ $choice ] = $choice;
		endwhile;
	else:
		$choice = 'block_title';
		$field['choices'][ $choice ] = $choice;
	endif;
    return $field;  
}
add_filter('acf/load_field/name=additional_packages', 'acf_load_additional_packages_choices');


// allow viewing of future posts
add_filter('get_post_status', function($post_status, $post) {
  if ($post->post_type == 'post' && $post_status == 'future') {
      return "publish";
  }
  return $post_status;
}, 10, 2);





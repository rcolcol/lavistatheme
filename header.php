<?php
/**
 * Header
 *
 * @package WordPress
 * @version 1.0.3
 */
?>

<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<meta name="apple-mobile-web-app-title" content="">

	<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>

	<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Ubuntu%3A300%2C400%2C700%7COpen+Sans%3A400%2C300&#038;ver=4.1' type='text/css' media='all' />

	<!-- <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

	<!-- <link rel="stylesheet" href="<?php //bloginfo('template_directory'); ?>/assets/css/slick.css"> -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?v=1.3.1">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	<![endif]-->

	<?php wp_head(); ?>

	<?php list($page, $class) = array_pad(explode('?', $_SERVER['REQUEST_URI']), 2, null);
				if ($page): $page = str_replace(array('/', '.php'), '', $page); endif;
				if ($class): $class = str_replace(array('='), '', $class); endif; ?>

</head>

<body id="<?php if($page): echo $page; endif; ?>" <?php if (function_exists('body_class')): body_class($class ); endif; ?>>

	<nav class="navbar navbar-expand-lg fixed-top navbar-light"">

		<div class="container">

			<a class="navbar-brand" href="<?php echo is_user_logged_in() ? home_url('/admin') : home_url() ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/lavista-logo.png" height="65" width="164"></a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

				<span class="navbar-toggler-icon"></span>

			</button>

			<div class="collapse navbar-collapse w-100 flex-md-column" id="navbarSupportedContent">

				<?php if (!is_user_logged_in()): ?>
					<ul class="navbar-nav flex-row mb-2 ml-auto" id="top_nav">
						<li class="nav-item">
							<div class="contact-number">Phone: <strong>1300 557 598</strong></div>
						</li>
					</ul>
				<?php endif; ?>

				<div class="menu_primary_wrapper" style="display: block;">
					<?php if (is_user_logged_in()) : ?>
						<div id="custom_nav_search" class="nav_menu_search">
							<?php
							$current_user = wp_get_current_user();
							dynamic_sidebar( 'widget-area-1' ); ?>

							<div class="nav__container">
								<a class="nav-item" href="<?php echo site_url() .'/admin/my-document-vault/' ?>">Document Vault</a>
								<a class="nav-item" href="<?php echo site_url() .'/admin/my-requests' ?>">My Requests</a>
								<a class="nav-item" href="<?php echo site_url() .'/admin/key-contacts/' ?>">Contact Us</a>
								<a id="auth__logout" data-uid="<?php echo $current_user->ID ?>" data-uname="<?php echo $current_user->display_name ?>" data-name="<?php echo $current_user->user_login ?>" data-role="<?php echo $current_user->roles[0] ?>" class="user_logout role_subsciber nav-item" href="<?php echo wp_logout_url() ?>">Logout</a>
							</div>
						</div>
					<?php endif;
					if(!is_user_logged_in()){
							wp_nav_menu( array(
								'menu'			 => 'Logged Out Menu',
								'menu_id'        => 'primary',
								'container'      => false,
								'depth'          => 2,
								'menu_class'     => 'navbar-nav ml-auto',
								'walker'         => new Bootstrap_NavWalker(),
								'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
							) );
						}
					?>

				</div>

			</div>

		</div>

	</nav>
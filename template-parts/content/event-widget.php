<?php 
/**
 * Event Widget Template
 */

$mobile = $args['mobile'];
$today = date("Y-m-d H:i:s"); 

$qargs = array(
    'post_type'       => 'event',
    'post_status'     => 'publish',
    'posts_per_page'  => 5,
    'meta_key'        => 'event_dates_event_start',
    'orderby'         => 'meta_value',
    'order'           => 'ASC',
    'meta_query'      => array(
        'relation' => 'OR',
        array(
            'key'     => 'event_dates_event_start',
            'compare' => '>=',
            'value'   => $today,
        ),
        array(
            'key'     => 'event_dates_event_end',
            'compare' => '>=',
            'value'   => $today,
        ),
    )
);

$events = new WP_Query( $qargs ); 

if ( $events->have_posts() ) : ?>
    <div class="widget_text widget widget_custom_html events_sb <?php echo $mobile ? 'hide_desktop' : 'hide_laptop' ?>">
        <h2 class="widgettitle"><a href="<?php echo home_url( '/admin/webinars-and-past-events/' ) ?>">Upcoming LaVista Events</a></h2>
        <div class="textwidget custom-html-widget">
            <div class="lv_posts" data-events>
                <?php
                while ( $events->have_posts() ) : $events->the_post(); 

                    $event_dates = get_field('event_dates');
                    $event_start = $event_dates['event_start'];
                    $startdate = $event_start ? DateTime::createFromFormat('Y-m-d H:i:s', $event_start) : ''; ?>

                    <div class="lv_post">
                        <div class="e_date"><span><?php echo $startdate->format('M'); ?></span><span><?php echo $startdate->format('j'); ?></span></div>
                        <div class="lv_post_content">
                            <h4 class="lv_post_title">
                                <a title="<?php echo get_the_title() ?>" target="_self" href="<?php echo get_permalink() ?>" rel="noopener"><?php the_title(); ?></a>
                            </h4>
                            <div class="lv_post_excerpt"><?php echo get_excerpt($post, get_the_content(), 80, 'Read More'); ?></div>
                        </div>
                    </div>

                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <div><a href="<?php echo home_url( '/admin/webinars-and-past-events/' ) ?>" class="btn accent all_btn">All Events</a></div>
        </div>
    </div>
<?php endif;


<?php
/**
 * Event Single
 */

$event_type = get_field('event_type');
  $event_dates = get_field('event_dates');
  $event_loc = get_field('event_location');
  $event_start = $event_dates['event_start'];
  $event_end = $event_dates['event_end'];
  $startdate = $event_start ? DateTime::createFromFormat('Y-m-d H:i:s', $event_start) : '';
  $enddate = $event_end ? DateTime::createFromFormat('Y-m-d H:i:s', $event_end) : '';
  $terms = get_the_terms( get_the_ID(), 'event_cat' );
  $term_list = wp_list_pluck( $terms, 'name' );
?>

<div class="post_item event">
    <div class="post_title">
        <h1><?php echo ucfirst(get_the_title()) ?></h1>
    </div>

    <div class="post_meta">
        <?php 
        if (count($term_list) > 0) : ?>
            <div class="post_cats">
            <span>Category: </span>
            <span>
                <?php
                foreach ($term_list as $key => $value) :
                    if ($key > 0) echo ', ' . $value ;
                    else echo $value;
                endforeach;
                ?>
            </span>
            </div>
            <?php
        endif;
        ?>
    </div>

    <div class="post_body">
        <?php 
        $banner = get_the_post_thumbnail(get_the_ID(), 'large', array('class' => 'event_img'));
        echo has_post_thumbnail() ? sprintf('<div class="event_img_wrapper">%s</div>', $banner) : '';
        the_content();
        ?>
        <div class="event_details">
            <h4>Event Details</h4>
            <?php
            $event_type_value = $event_type !== '-- Select --' ? sprintf('<span>%s</span>', $event_type) : '';
            echo $event_type !== '-- Select --' ? sprintf('<div class="event_type"><span>Type: </span> %s</div>', $event_type_value) : '';

            $event_address_value = $event_loc ? sprintf('<span>%1$s</span>', $event_loc['address']) : '';
            echo $event_loc ? sprintf('<div class="event_address">Address: %1$s %2$s</div>', lavista_svg_icon('map-pin', 18), $event_address_value) : '';

            $time_start = $event_start ? sprintf('<span class="time_start">%s</span>', $startdate->format('d/m/Y g:ia')) : '';
            $time_end = $event_end ? sprintf('<span class="time_end">%s</span>', $enddate->format('d/m/Y g:ia')) : '';
            echo $event_dates ? sprintf('<div class="event_times">Date: %1$s %2$s %3$s</div>', lavista_svg_icon('clock', 18),  $time_start, $event_end ? '- ' . $time_end : '') : '';

            get_template_part('template-parts/content/event', 'map');
            ?>
        </div>
    </div>
</div> <!-- end post_item -->
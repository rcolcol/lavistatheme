<?php
/**
 * Event Map
 */

$event_loc = get_field('event_location');

if ( $event_loc ) : ?>
    <div class="dealer_map_display">
        <div class='map-container'>
            <div class='wrap'>
                <div class='acf-map'>
                    <div style="opacity:0;" class="marker" data-lat="<?php echo $event_loc['lat']; ?>" data-lng="<?php echo $event_loc['lng']; ?>">
                        <h4><?php the_title(); ?></h4>
                        <p><em><?php echo esc_html( $event_loc['address'] ); ?></em></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
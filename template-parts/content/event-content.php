<?php
/**
 * Event content
 */

  $event_type = get_field('event_type');
  $event_dates = get_field('event_dates');
  $event_loc = get_field('event_location');
  $event_start = $event_dates['event_start'];
  $event_end = $event_dates['event_end'];
  $startdate = $event_start ? DateTime::createFromFormat('Y-m-d H:i:s', $event_start) : '';
  $enddate = $event_end ? DateTime::createFromFormat('Y-m-d H:i:s', $event_end) : '';
  $terms = get_the_terms( get_the_ID(), 'event_cat' );
  $term_list = wp_list_pluck( $terms, 'name' );
?>

<div class="post_item event">
  <?php 
  if ($event_start) : ?>
    <div class="disp_left">
      <?php echo sprintf('<div class="disp_date"><span class="smonth">%1$s</span><span class="sdate">%2$s</span><span class="sday">%3$s</span></div>', $startdate->format('M'), $startdate->format('j'), $startdate->format('l')); ?>
    </div>
    <?php
  endif; ?>

  <div class="disp_right">

    <div class="post_title">
      <h4><a href="<?php echo get_permalink() ?>"><?php echo ucfirst(get_the_title()) ?></a></h4>
    </div>

    <div class="post_meta">
      <?php 
      if (count($term_list) > 0) : ?>
        <div class="post_cats">
          <span>Category: </span>
          <span>
            <?php
              foreach ($term_list as $key => $value) :
                if ($key > 0) echo ', ' . $value ;
                else echo $value;
              endforeach;
            ?>
          </span>
        </div>
        <?php
      endif;

      $event_type_value = $event_type !== '-- Select --' ? sprintf('<span>%s</span>', $event_type) : '';
      echo $event_type !== '-- Select --' ? sprintf('<div class="event_type"><span>Type: </span> %s</div>', $event_type_value) : '';

      $time_start = $event_start ? sprintf('<span class="time_start">%s</span>', $startdate->format('g:ia')) : '';
      $time_end = $event_end ? sprintf('<span class="time_end">%s</span>', $enddate->format('g:ia')) : '';
      echo $event_dates ? sprintf('<div class="event_times">%1$s %2$s %3$s</div>', lavista_svg_icon('clock', 18),  $time_start, $event_end ? '- ' . $time_end : '') : '';

      $event_address_value = $event_loc ? sprintf('<span>%1$s</span>', $event_loc['address']) : '';
      echo $event_loc ? sprintf('<div class="event_address">%1$s %2$s</div>', lavista_svg_icon('map-pin', 18), $event_address_value) : '';
      ?>
    </div>

    <div class="post_body">
      <?php 
      $banner = get_the_post_thumbnail(get_the_ID(), 'large', array('class' => 'event_img'));
      echo has_post_thumbnail() ? sprintf('<div class="event_img_wrapper">%s</div>', $banner) : '';
      echo get_excerpt($post, get_the_content(), 150, 'Read More'); 
      ?>
    </div>
  </div>
</div> <!-- end post_item -->
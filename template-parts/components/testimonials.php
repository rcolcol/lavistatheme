<?php
/**
 * Testimonials
 *
 * @package: WordPress
 * @version: 1.0.0
 * @license: MIT
 * Date: 2025-02-03
 */


?>

<div class="container" id="reviews">
    <div class="row mb-4">
        <div class="col-md-12 about-text clients">
            <h2>Testimonials</h2>
            <div class="client-reviews" id="client-reviews">
                <?php if( have_rows('our_reviews') ): ?>
                    <?php while( have_rows('our_reviews') ): the_row();
                        $review_logo = get_sub_field('review_logo'); $review_name = get_sub_field('review_name');
                        $review_heading = get_sub_field('review_heading'); $review_body = get_sub_field('review_body');
                    ?>
                <div>
                        <?php if($review_name && $review_body) :
                            if ($review_logo) : ?>
                            <div class="review_logo"><img src="<?php echo $review_logo; ?>"/></div>
                            <?php else: ?>
                            <div class="review_logo_default"><img src="<?php echo get_bloginfo('stylesheet_directory').'/assets/images/iconmonstr-user-96x96.png'; ?>"/></div>
                            <?php endif; ?>
                            <p class="review_heading" <?php if($review_heading) :?> style="display: block;" <?php else : ?> style="display: none;" <?php endif; ?> ><strong><?php echo "\"" . $review_heading . "\""; ?></strong></p>
                            <p class="review_body"><?php echo $review_body; ?></p>
                            <h4 class="review_name">- <?php echo $review_name; ?></h4>
                        <?php endif; ?>
                </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * How we help
 *
 * @package: WordPress
 * @version: 1.0.0
 * @license: MIT
 * Date: 2025-01-31
 */

?>

<div class="container how-we-help">
    <div class="row">
        <div class="col-md-12">
            <h2><?php echo get_field('how_we_can_help_title'); ?></h2>
        </div>
    </div>
    <div class="row">
        <?php if( have_rows('help_options') ): ?>
            <?php while( have_rows('help_options') ): the_row(); ?>
                <div class="col-sm col-12">
                    <div class="media">
                        <h4 class="mr-3"><i class="<?php the_sub_field('category_icon'); ?>"></i></h4>
                        <div class="media-body">
                            <h4><?php the_sub_field('category_title'); ?></h4>
                        </div>
                    </div>
                    <?php the_sub_field('category_text'); ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>

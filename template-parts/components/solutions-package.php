<?php
/**
 * Solutions Package
 *
 * @package: WordPress
 * @version: 1.0.0
 * @license: MIT
 * Date: 2025-01-31
 */

?>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="solutions-package">
                <h4><i class="<?php echo get_field('get_started_icon'); ?> mb-3" style="font-size:2em;"></i><br><?php echo get_field('get_started_title_text'); ?></h4>
                <div class="solution-text start">
                    <p><?php echo get_field('get_started_paragraph_text'); ?></p>
                </div>
                <a href="<?php echo get_page_link(129); ?>" class="btn btn-outline-light mb-3"><?php echo get_field('get_started_button_text'); ?></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="solutions-package">
                <h4><i class="<?php echo get_field('manage_icon'); ?> mb-3" style="font-size:2em;"></i><br><?php echo get_field('manage_text'); ?></h4>
                <div class="solution-text start">
                    <p><?php echo get_field('manage_paragraph_text'); ?></p>
                </div>
                <a href="<?php echo get_page_link(37); ?>" class="btn btn-outline-light mb-3"><?php echo get_field('manage_button_text'); ?></a>
            </div>
        </div>
    </div>
</div>

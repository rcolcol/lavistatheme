<?php
/**
 * filename
 *
 * @package: WordPress
 * @version: 1.0.2
 * @license: MIT
 * Date: 2025-01-31
 */
?>


<div class="container" id="who-is-lavista">
    <div class="row">
        <div class="col-md-12 about-text">
            <h1>Who is LaVista?</h1>
            <div class="card-deck profiles mb-3">
                <?php if( have_rows('key_personal') ): ?>
                    <?php while( have_rows('key_personal') ): the_row(); ?>
                        <div class="card">
                            <img class="card-img-top" src="<?php the_sub_field('image'); ?>" alt="Todd Kardash">
                            <div class="card-body">
                                <h4 class="card-title"><?php the_sub_field('name'); ?></h4>
                                <p class="card-text"><?php the_sub_field('description'); ?></p>
                                <p class="card-text">
                                    <div class="btn-group" role="group">
                                        <?php if( get_sub_field('email') ): ?>
                                            <a href="mailto:<?php the_sub_field('email'); ?>" class="btn btn-secondary" target="_blank">Email</a>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('linkedin') ): ?>
                                            <a href="<?php the_sub_field('linkedin'); ?>" class="btn btn-primary" target="_blank">LinkedIn</a>
                                        <?php endif; ?>
                                    </div>
                                </p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php
        if (!is_page(97)) : ?>
            <div class="col-md-12 text-center">
                <a href="<?php echo get_page_link(97); ?>" class="btn btn-primary">Learn more about LaVista</a>
            </div>
            <?php
        endif; ?>
    </div>
</div>


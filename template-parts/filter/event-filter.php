<?php
/**
 * Event Filter
 */

$query = $args['query'];
$posts = $query->posts;
$array_loc = [];

// foreach ($posts as $key => $item) :
    // $event_loc = get_field('event_location', $item->ID);
    // $event_state = $event_loc ? (array_key_exists('state', $event_loc) ? $event_loc['state'] : NULL) : NULL;

    // if (!isset($event_state)) continue;
    // if (in_array($event_state, $array_loc)) continue;

    // $array_loc[str_replace(' ', '-', strtolower(trim($event_state)))] = $event_state;
// endforeach;

?>

<div class="post_filter" data-post-filter>
    <select name="event_type" class="event_type">
        <option value="0">Select Event Type</option>
        <option value="virtual">Virtual</option>
        <option value="in-person">In Person</option>
    </select>

    <!-- <select name="event_location" class="event_location" style="display: none;"> -->
        <!-- <option value="0">Select Location</option> -->
        <?php
        // if (count($array_loc) > 0) :
        //     asort($array_loc);
        //     foreach ($array_loc as $key => $loc) : ?>
                <!-- <option value="<?php //echo $key ?>"><?php //echo $loc ?></option> -->
                <?php
            // endforeach;
        // endif;
        ?>
    <!-- </select> -->

    <button class="filter_btn" name="filter">Search</button>
    <button class="filter_btn" name="reset">Reset</button>
</div>
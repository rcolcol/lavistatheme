<?php
/**
 * Index
 *
 * @package WordPress
 * @version 1.0.2
 */

get_header();?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

		<?php if(is_front_page()) : ?>
			<div class="hero-image" style="background-image: url(<?php echo get_field('image'); ?>);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1><?php echo get_field('welcome_message'); ?></h1>
							<h3><?php echo get_field('call_to_action_text'); ?></h3>
							<button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#schedulecallback"><?php echo get_field('button_text'); ?></button>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 about-text">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="solutions-package">
							<h4><i class="<?php echo get_field('get_started_icon'); ?> mb-3" style="font-size:2em;"></i><br><?php echo get_field('get_started_title_text'); ?></h4>
							<div class="solution-text start">
								<p><?php echo get_field('get_started_paragraph_text'); ?></p>
							</div>
							<a href="<?php echo get_page_link(129); ?>" class="btn btn-outline-light mb-3"><?php echo get_field('get_started_button_text'); ?></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="solutions-package">
							<h4><i class="<?php echo get_field('manage_icon'); ?> mb-3" style="font-size:2em;"></i><br><?php echo get_field('manage_text'); ?></h4>
							<div class="solution-text start">
								<p><?php echo get_field('manage_paragraph_text'); ?></p>
							</div>
							<a href="<?php echo get_page_link(37); ?>" class="btn btn-outline-light mb-3"><?php echo get_field('manage_button_text'); ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="container how-we-help">
				<div class="row">
					<div class="col-md-12">
						<h2><?php echo get_field('how_we_can_help_title'); ?></h2>
					</div>
				</div>
				<div class="row">
					<?php if( have_rows('help_options') ): ?>
					 	<?php while( have_rows('help_options') ): the_row(); ?>
							<div class="col-sm col-12">
						    	<div class="media">
								    <h4 class="mr-3"><i class="<?php the_sub_field('category_icon'); ?>"></i></h4>
								    <div class="media-body">
								    	<h4><?php the_sub_field('category_title'); ?></h4>
								  	</div>
								</div>
							    <?php the_sub_field('category_text'); ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="container" id="who-is-lavista">
				<div class="row">
					<div class="col-md-12 about-text">
						<h2>Who is LaVista?</h2>
						<div class="card-deck mb-3">
							<?php if( have_rows('key_personal') ): ?>
							 	<?php while( have_rows('key_personal') ): the_row(); ?>
									<div class="card">
										<img class="card-img-top" src="<?php the_sub_field('image'); ?>" alt="Todd Kardash">
										<div class="card-body">
											<h4 class="card-title"><?php the_sub_field('name'); ?></h4>
											<p class="card-text"><?php the_sub_field('description'); ?></p>
											<p class="card-text">
												<div class="btn-group" role="group">
													<?php if( get_sub_field('email') ): ?>
														<a href="mailto:<?php the_sub_field('email'); ?>" class="btn btn-secondary" target="_blank">Email</a>
													<?php endif; ?>
													<?php if( get_sub_field('linkedin') ): ?>
														<a href="<?php the_sub_field('linkedin'); ?>" class="btn btn-primary" target="_blank">LinkedIn</a>
													<?php endif; ?>
												</div>
											</p>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
						<?php echo get_field('team_details'); ?>
						<div class="row awards">
							<?php if( have_rows('awards') ): ?>
							 	<?php while( have_rows('awards') ): the_row(); ?>
							 		<div class="col-sm col-6"><img src="<?php the_sub_field('award_images'); ?>"></div>
						 		<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-12 text-center">
						<a href="<?php echo get_page_link(97); ?>" class="btn btn-primary">Learn more about LaVista</a>
					</div>
				</div>
			</div>

			<div class="container" id="partners">
				<div class="row mb-4">
					<div class="col-md-12 about-text clients">
					    <h2>Partners we work with</h2>
					    <ul class="client-list">
					    	<?php if( have_rows('our_partners') ): ?>
							 	<?php while( have_rows('our_partners') ): the_row();
									$post_id = attachment_url_to_postid( get_sub_field('partner_logo') );
									$img_url = wp_get_attachment_image_src($post_id, 'medium')[0]; ?>
							 		<?php if( get_sub_field('partner_link') ): ?>
							 			<li><a href="<?php the_sub_field('partner_link'); ?>" target="_blank"><img src="<?php echo $img_url; ?>"></a></li>
						 			<?php else: ?>
						 				<li><img src="<?php echo $img_url; ?>"></li>
							 		<?php endif; ?>
						 		<?php endwhile; ?>
							<?php endif; ?>
					    </ul>
					</div>
				</div>
			</div>

			<?php // get_template_part( 'template-parts/components/testimonials' ); ?>

		<?php elseif(is_page(3)) : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12 about-text" style="margin-bottom:40px;">
						<?php echo get_field('privacy_policy'); ?>
					</div>
				</div>
			</div>

		<?php elseif(is_page(479)) : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12 about-text" style="margin-bottom:40px;">
						<?php echo get_field('terms_of_use'); ?>
					</div>
				</div>
			</div>

		<?php elseif(is_page(97)) : ?>
			<div class="pb-3">
				<?php get_template_part( 'template-parts/components/who-is-lavista' ); ?>
				<div class="container">
					<div class="row">
						<div class="col-md-12 about-text" style="margin-bottom:40px;">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>

		<?php elseif(is_page(17010)) : ?>
			<div class="pb-5">
				<div class="container">
					<div class="row">
						<div class="col-md-12 about-text" style="margin-bottom:40px;">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php get_template_part( 'template-parts/components/solutions-package' ); ?>
				<?php get_template_part( 'template-parts/components/how-we-help' ); ?>
			</div>

		<?php elseif('463' == wp_get_post_parent_id($post->ID)) : ?>
			<?php function human_filesize($bytes, $decimals = 2) {
					$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
					$factor = floor((strlen($bytes) - 1) / 3);
					return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
				}

				function timeBetween($start, $end=-1){
					if($end < 0) $end = time();
					$SECOND = 1;
					$MINUTE = 60 * $SECOND;
					$HOUR = 60 * $MINUTE;
					$DAY = 24 * $HOUR;
					$WEEK = 7 * $DAY;
					$MONTH = 30 * $DAY;
					$YEAR = 365 * $DAY;
					$increments = [
						[$SECOND, 'second'],
						[$MINUTE, 'minute'],
						[$HOUR, 'hour'],
						[$DAY, 'day'],
						[$WEEK, 'week'],
						[$MONTH, 'month'],
						[$YEAR, 'year']

					];

					$diff = $end - $start;
					$plural = '';
					$units = ceil($diff/$increments[count($increments)-1][0]);
					$unit = $increments[count($increments)-1][1];
					for($i = 1; $i < count($increments); $i++){
						if($increments[$i-1][0] <= $diff && $diff < $increments[$i][0]){
							$units = ceil($diff/$increments[$i-1][0]);
							$unit = $increments[$i-1][1];
							break;
						}
					}

					if($units > 1) $plural = 's';
					return sprintf("%d %s%s", $units, $unit, $plural);
				} ?>

			<?php if (is_user_logged_in()) { ?>
				<div class="main_content_wrapper">
					<div id="sidebar_left" class="sidebars">
					<?php
					wp_nav_menu( array(
						'menu'		     => 'News & updates Menu Sidebar',
						'menu_id'        => 'primary',
						'container'      => false,
						'depth'          => 2,
						'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
						'walker'         => new Bootstrap_NavWalker(),
						'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
					) ); ?>
					<h5>Licensee</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Licensee Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) ); ?>
					<h5>Client Advice</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Client Advice Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) ); ?>
					<h5>Research</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Research Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) ); ?>
					<h5>Practice Management</h5>
					<?php
						wp_nav_menu( array(
							'menu'		     => 'Practice Management Menu Sidebar',
							'menu_id'        => 'primary',
							'container'      => false,
							'depth'          => 2,
							'menu_class'     => 'navbar-nav ml-auto custom-loggedin-menu-sidebar',
							'walker'         => new Bootstrap_NavWalker(),
							'fallback_cb'    => 'Bootstrap_NavWalker::fallback'
						) );
						?>
					</div>

					<div class="main_content">
						<div class="container admin_sub_page">
							<div class="row">
								<div class="col-md-12 about-text" style="margin-bottom:40px;">
									<div id="back_history" class="back_history">
										<a href="<?php echo site_url() . '/admin' ?>"><i class="fas fa-long-arrow-alt-left"></i>Back to News & updates</a>
									</div>
									<h1 class="page_heading"><?php the_title(); ?></h1>

									<?php
									$user_id = get_current_user_id();
									$user_level = get_field('member_level', 'user_'.$user_id);
									$member_access = get_field('member_access');

									if (!empty($user_level) || isset($user_level)) :
										if (in_array($user_level[0], (array) $member_access['member_level'])) :
											echo '<div class="row">';
												echo '<div class="col md-12">';
													echo '<h5 class="text-center">Content is not available.</h5>';
												echo '</div>';
											echo '</div>';

										else : ?>

										<?php wpdm_user_has_access(127); ?>
										<?php the_content(); ?>

										<div class="card-deck-wrapper">
											<?php //$loop_counter = 0;
											// check if the flexible content field has rows of data
											if( have_rows('add_new_block') ):
												// loop through the rows of data
												while ( have_rows('add_new_block') ) : the_row();
													// if( $loop_counter % 2 == 0) : echo '</div><div class="card-deck">'; endif;
													// if( $loop_counter % 2 == 0) : echo '<div class="card_wrapper">';
														// $loop_counter++;

														if( get_row_layout() == 'free_text' ): ?>
															<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck full_width"><div class="card bg-light mb-3">';
															elseif( get_sub_field('block_width') == 'half' ): echo '<div class="card-deck half_width"><div class="card bg-light mb-3">';
															else: echo '<div class="card-deck default_width"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
																<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
																<div class="card-body">
																	<?php the_sub_field('block_content'); ?>
																	<?php if( get_sub_field('page_links_to') != '' ):
																		echo '<a class="btn btn-primary" href="';
																		the_sub_field('page_links_to');
																		echo '" role="button">Read more</a>';
																	endif; ?>
																</div>
															</div>
														</div>

														<?php elseif( get_row_layout() == 'schedule_a_service' ): ?>
															<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">';
															else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
																<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
																<div class="card-body">
																	<?php echo do_shortcode( '[contact-form-7 id="516" title="Schedule a service"]' ); ?>
																</div>
															</div>
														</div>

														<?php elseif( get_row_layout() == 'event_calendar' ): ?>
															<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">';
															else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
																<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
																<div class="card-body">
																	<?php if( have_rows('event_details') ): ?>
																		<?php while( have_rows('event_details') ): the_row(); ?>
																			<p class="card-text"><strong><?php the_sub_field('event_date'); ?></strong> - <?php the_sub_field('event_name'); ?><br><?php the_sub_field('event_description'); ?></p>
																		<?php endwhile; ?>
																	<?php endif; ?>
																</div>
															</div>
														</div>

														<?php elseif( get_row_layout() == 'advice_documents' ): ?>
															<?php if( get_sub_field('block_width') == 'full' ): echo '<div class="card-deck"><div class="card bg-light mb-3">';
															else: echo '<div class="card-deck"><div class="card bg-light mb-3" style="max-width:540px;">'; endif; ?>
																<div class="card-header font-weight-bold"><?php the_sub_field('block_title'); ?></div>
																<div class="card-body">
																	<?php if( have_rows('documents') ): ?>
																		<?php while( have_rows('documents') ): the_row(); ?>
																			<?php $attachment_id = get_sub_field('the_document');
																				$filesize = filesize( get_attached_file( $attachment_id ) );
																				$last_edited = filectime( get_attached_file( $attachment_id ) ); ?>
																			<p class="card-text"><a href="<?php echo wp_get_attachment_url( $attachment_id ); ?>" target="_blank"><?php the_sub_field('document_title'); ?></a> - <?php echo human_filesize($filesize); ?> - Last edited <?php echo timeBetween($last_edited); ?></p>
																		<?php endwhile; ?>
																	<?php endif; ?>
																</div>
															</div>
														</div>

														<?php elseif( get_row_layout() == 'add_standard_paragraph' ):?>
															<!-- The block of content -->
															<div class="card-deck">
																<?php the_sub_field('block_content'); ?>
															</div>

														<?php else: ?>
															<div>
																<h4>No content added.</h4>
															</div>
														<?php endif; ?>
													<!-- </div> -->
													<?php // endif;
													// $loop_counter++; ?>
												<?php endwhile; ?>
											<?php endif; ?>
										</div>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

					<div id="sidebar_right" class="sidebars">
						<!-- Good to know  -->
						<?php $menuname = 'Latest Compliance Updates';
							if (wp_get_nav_menu_items($menuname)) : ?>
								<div class="widget menu_widget">
									<h2 class="widgettitle"><?php echo $menuname ?></h2>
									<?php
										wp_nav_menu( array(
											'menu'					 => $menuname,
											'menu_id'        => 'gtk_menu',
											'container'      => false,
											'depth'          => 2,
											'menu_class'     => 'navbar-nav ml-auto',
											'walker'         => new Bootstrap_NavWalker(),
											'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
										) );
									?>
								</div>
								<?php
							endif; ?>

						<?php

						get_template_part( 'template-parts/content/event', 'widget', array('mobile' => false) );

						dynamic_sidebar('Sidebar Widget Area 2'); ?>

					</div>
				</div>

				<?php } else { ?>
					<div class="container wow">
						<div class="row">
							<div class="col-md-12 about-text" style="margin-bottom:40px;">
								<?php echo wpdm_login_form(); ?>
							</div>
						</div>
					</div>
				<?php } ?>

		<?php else : ?>
			<div class="container admin_sub_page">
				<div class="row">
					<div class="col-md-12 about-text" style="margin-bottom:40px;">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

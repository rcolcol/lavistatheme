<?php
/**
 * Lavista Functions
 *
 *
 * @package WordPress
 * @version 1.0.3
 */

function dump($args=null) {
  echo '<pre>';
  print_r($args);
  echo '</pre>';
}

function add_slug_body_class($classes) {
  global $post;
  if (isset($post)) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  $classes[] = 'md';
  return $classes;
}
add_filter('body_class', 'add_slug_body_class');

require get_template_directory() . '/inc/md-supports.php';

function admin_scripts() {
  wp_enqueue_style('md-admin-styles', get_template_directory_uri().'/assets/css/md-admin.css?' . date_i18n(__( 'AYmd-h:i:s' ) ), array(), wp_get_theme()->get('Version'), 'all');
  wp_enqueue_script('md-admin-scripts', get_template_directory_uri() . '/assets/js/md-admin.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get('Version'), true);

  if (preg_match('/term.php/i', $_SERVER['REQUEST_URI']) && preg_match('/taxonomy=group&tag_ID/i', $_SERVER['REQUEST_URI'])) {
    wp_enqueue_script( 'ajax-shared',  get_template_directory_uri() . '/assets/js/ajax-admin.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get('Version'), true);
    wp_localize_script( 'ajax-shared', 'ajaxsharedpost', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'ajaxnonce' => wp_create_nonce( 'ajax_post_validation' ),
    ));
  }
}
add_action('admin_enqueue_scripts', 'admin_scripts');


function enqueue_child_theme_styles() {
  wp_enqueue_script('feather-icons', 'https://unpkg.com/feather-icons', array(), wp_get_theme()->get('Version'), true);

  wp_enqueue_script( 'mapinit-script', get_template_directory_uri() . '/assets/js/mapinit.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get( 'Version' ), true );
  wp_enqueue_script( 'gmap-apikey', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBVYB_M-_7PLPop9ydYFVosVBQpx304YCo&callback=mapInit', array(), wp_get_theme()->get( 'Version' ), true );
  
  wp_enqueue_style('md-styles', get_template_directory_uri() . '/assets/css/md-main.css?' . date_i18n(__( 'AYmd-h:i:s' ) ), array(), wp_get_theme()->get('Version'), 'all');
  wp_enqueue_script('md-scripts', get_template_directory_uri() . '/assets/js/md-main.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get('Version'), true);
  wp_localize_script( 'md-scripts', 'mainParams', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    'nonce' => wp_create_nonce( 'ajax_post_validation' ),
  ) );

  if( is_user_logged_in() ) { 
    wp_enqueue_style('loggedin-user-style', get_template_directory_uri() . '/assets/css/loggedin-user.css?' . date_i18n(__( 'AYmd-h:i:s' ) ), array(), wp_get_theme()->get('Version'), 'all');
    wp_enqueue_script('loggedin-user-script', get_template_directory_uri() . '/assets/js/loggedin-user.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get('Version'), true);
  }
  
  if (is_user_logged_in() && (is_page('my-document-vault') && preg_match('/adb_page=edit-package/i', $_SERVER['REQUEST_URI']))) {
    wp_enqueue_script( 'ajax-calls', get_template_directory_uri() . '/assets/js/ajax-calls.js?' . date_i18n(__( 'AYmd-h:i:s' ) ), array('jquery'), wp_get_theme()->get('Version'), true);
    wp_localize_script( 'ajax-calls', 'ajaxcalls', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'ajaxnonce' => wp_create_nonce( 'ajax_post_validation' ),
    ) );
  }
}
add_action('wp_enqueue_scripts', 'enqueue_child_theme_styles');



function lavista_include_ga_code() {
  $code = "<!-- Google tag (gtag.js) -->
  <script async src='https://www.googletagmanager.com/gtag/js?id=G-Z05KP99872'></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-Z05KP99872');
  </script>";
  echo $code;
}
add_action('wp_head','lavista_include_ga_code', 5);


function ajax_calls() {
  $post_id = $_POST['post_id'];
  $user_name = $_POST['user_name'];
  $term_name = $_POST['term_name'];
  $term_id = $_POST['term_id'];
  $admin = ['administrator'];

  $valueUser1 = get_post_meta($post_id, '__wpdm_access', true);
  $valueUser2 = get_post_meta($post_id, '__wpdm_user_access', true);

  if (! empty($user_name)) {
    wp_set_post_terms( $post_id, $term_id, 'group', true );
    $user_name = maybe_serialize( $user_name );
    update_post_meta($post_id, '__wpdm_access', $admin);
    update_post_meta($post_id, '__wpdm_user_access', $user_name);
  } else {
    update_post_meta($post_id, '__wpdm_access', $admin);
    delete_post_meta($post_id, '__wpdm_user_access',);
  }

  wp_die();
}
add_action( 'wp_ajax_ajax_calls', 'ajax_calls' );
add_action('wp_ajax_nopriv_ajax_calls', 'ajax_calls');


function my_ajax_shared_post() {

  if (! empty($_POST['post_idin'])) {
    $args_ex = array(
      'post_type' => 'wpdmpro',
      'post_status' => 'publish',
      'posts_per_page' => 999,
      'update_post_meta_cache' => false,
      'author' => $_POST['post_idex'],
    );
    $events_ex = new WP_Query( $args_ex ); 

    if ( $events_ex->have_posts() ) {
      while ( $events_ex->have_posts() ) {
        $events_ex->the_post(); 

        $id = get_the_ID();
        $term_name = $_POST['term_name'];

        if (is_object_in_term($id, 'group', $term_name)) {
          wp_set_post_terms($id, null, 'group');
          delete_post_meta($id, '__wpdm_user_access', '');
        } 

        if (! has_term('', 'group')) {
          delete_post_meta($id, '__wpdm_user_access', '');
        }
      }
      wp_reset_postdata();
    } 

    $args_in = array(
      'post_type' => 'wpdmpro',
      'post_status' => 'publish',
      'posts_per_page' => 999,
      'update_post_meta_cache' => false,
      'author' => $_POST['post_idin'],
    );
    $events_in = new WP_Query( $args_in ); 

    if ( $events_in->have_posts() ) {
      while ( $events_in->have_posts() ) {
        $events_in->the_post(); 

        $id = get_the_ID();

        $valueUser1 = get_post_meta($id, '__wpdm_access', true);
        $valueUser2 = get_post_meta($id, '__wpdm_user_access', true);
        $theusers = $_POST['user_id'];
        $term_name = $_POST['term_name'];
        $term_id = $_POST['term_id'];
        $admin = ['administrator'];

        wp_set_post_terms( $id, null, 'group' );
        wp_set_post_terms( $id, $term_id, 'group', true );
        update_post_meta($id, '__wpdm_access', $admin);
        update_post_meta($id, '__wpdm_user_access', $theusers);
      }
      wp_reset_postdata();
    }
  } else {
    $args_exu = array(
      'post_type' => 'wpdmpro',
      'post_status' => 'publish',
      'posts_per_page' => 999,
      'update_post_meta_cache' => false,
      'tax_query' => array(
        array(
          'taxonomy' => 'group',
          'field'    => 'term_id',
          'terms'    => $_POST['term_id'],
        ),
      ),
    );
    $events_exu = new WP_Query( $args_exu ); 

    if ( $events_exu->have_posts() ) {
      while ( $events_exu->have_posts() ) {
        $events_exu->the_post(); 

        $id = get_the_ID();
        $term_name = $_POST['term_name'];

        if (is_object_in_term($id, 'group', $term_name)) {
          wp_set_post_terms($id, null, 'group');
          delete_post_meta($id, '__wpdm_user_access', '');
        } 
      }
      wp_reset_postdata();
    }
  }

	wp_die();
}
add_action( 'wp_ajax_my_ajax_shared_post', 'my_ajax_shared_post' );
add_action( 'wp_ajax_nopriv_my_ajax_shared_post', 'my_ajax_shared_post' );


function md_wpdmpro_group_members() {
  $user = wp_get_current_user();

  if ( is_user_logged_in() && ($user->roles[0] == 'subscriber' || $user->roles[0] == 'administrator') && (is_page('my-document-vault') && preg_match('/adb_page=edit-package/i', $_SERVER['REQUEST_URI'])) ) {
    $terms = get_terms( array(
      'taxonomy'   => 'group', // Swap in your custom taxonomy name
      'hide_empty' => false, 
    )); 
    
    echo '<div style="display: none;" id="wpdmpro_group_term" class="wpdmpro_group_term">';
      foreach( $terms as $term ) {      
        $groups = get_field('group_users', $term);
        if( $groups ) {
          echo '<ul class="wpdmpro_group_term_list" data-term-slug="'.$term->slug.'" data-term-id="'.$term->term_id.'" data-term-name="'.$term->name.'">';
          foreach( $groups as $group ) {
            $member = $group['member_name'];
            echo '<li class="group_member" data-member-id="'.$member['ID'].'"></li>';
          }
          echo '</ul>';
        }
      }
    echo '</div>';

    $subs_users = get_users( [ 'role__in' => [ 'subscriber' ] ] );
    echo '<script>const userNames = [';
    foreach ( $subs_users as $user ) {
      echo '"'.esc_html( $user->user_login ).' (' .esc_html( $user->display_name ) . ') '.esc_html( $user->ID ).'",';
    }
    echo ']</script>';
  }
}
add_action('wp_head', 'md_wpdmpro_group_members');


function register_my_menu() {
  register_nav_menus(array(
    'top_menu' => __('Top Menu', 'theme-slug'),
    'main_menu' => __('Main Menu', 'theme-slug'),
    'footer_menu' => __('Footer Menu', 'theme-slug'),
    'bottom_menu' => __('Bottom Menu', 'theme-slug'),
    'logged_in_menu' => __('Logged In Menu', 'theme-slug'),
    'logged_out_menu' => __('Logged Out Menu', 'theme-slug'),
  ));
}
add_action( 'after_setup_theme', 'register_my_menu' );


function wpdocs_theme_slug_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Widget Area 1', 'textdomain' ),
    'id'            => 'widget-area-1',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Widget Area 2', 'textdomain' ),
    'id'            => 'widget-area-2',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Sidebar Widget Area 1', 'textdomain' ),
    'id'            => 'sidebar-widget-area-1',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Sidebar Widget Area 2', 'textdomain' ),
    'id'            => 'sidebar-widget-area-2',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Sidebar Widget Area 3', 'textdomain' ),
    'id'            => 'sidebar-widget-area-3',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Widget User Group', 'textdomain' ),
    'id'            => 'widget-user-group',
    'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );


function add_redirect_is_not_admin() {
  if ( is_user_logged_in() && is_front_page() ) {
    wp_safe_redirect(home_url('/admin'));
    exit;
  }

  if ( is_user_logged_in() && is_page('login-page') ) {
    wp_safe_redirect(home_url('/admin'));
    exit;
  }
}
add_action('template_redirect', 'add_redirect_is_not_admin');


function exclude_menu_item($items, $args) {
  $user_id = get_current_user_id();
  $user_level = get_field('member_level', 'user_'.$user_id);
  
  if ($args->menu != 'Logged In Menu Sidebar')  
  return $items;
  
  if (empty($user_level) || !isset($user_level)) return;
  
  if (count($user_level) > 0) :
    foreach ($user_level as $key => $level) :

      if (count((array) $items) > 0) :
        foreach ($items as $key => $menu) :
          $hide_menu = get_field('hide_menu_option', $menu->ID);
          
          if (empty($hide_menu)) continue; 

          $options = $hide_menu['member_level'];

          if (count($options) < 1) {
            continue;
          }

          if (in_array($level, $options)) {
            unset($items[$key]);
          }

        endforeach;
      endif;
    endforeach;
  endif;

  return $items;
}
add_filter( 'wp_nav_menu_objects', 'exclude_menu_item', 10, 2);


function get_allowed_wpdm_posts() {
  $uid = get_current_user_id();
  $uroles = wp_get_current_user()->roles[0];
  $dname = wp_get_current_user()->display_name;
  $uloginname = wp_get_current_user()->user_login;
  
  $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
  $args = array(
    'post_type' 		  => 'wpdmpro',
    'posts_per_page'	=> 50,
    'paged'           => $paged,
    // 'meta_key'   		  => '__wpdm_access',
    'meta_key'   		  => '__wpdm_user_access',
    'orderby'    		  => 'meta_value_num',
      'order'      		=> 'ASC',
      'meta_query' 		=> array(
          array(
            // 'key'     => '__wpdm_access',
            'key'     => '__wpdm_user_access',
            // 'value'  	=> $uroles,
            'value'  	=> $uloginname,
            'compare' => 'LIKE',
          ),
      ),
  );
  
  $query1 = new WP_Query( $args );
  
  
  function get_the_custom_wpdmpro_template($default_icon, $icon, $dname, $size, $files) { ?>
    <div class="w3eden">
      <div class="link-template-default card mb-2 shared_wpdm" data-dl-name="<?php echo $dname; ?>">
        <div class="card-body">
          <div class="media">
            <div class="mr-3 img-48"><img alt="Icon" class="wpdm_icon" src="<?php echo (empty($icon)) ? $default_icon : $icon; ?>"></div>
            <div class="media-body">
              <h3 class="package-title"><a href="<?php echo get_the_permalink() ?>"><?php echo get_the_title() ?></a></h3>
              <div class="text-muted text-small">
                <?php 
                if (!empty($files)) : ?>
                  <i class="fas fa-copy"></i> <?php echo count($files).' file(s)' ?> <i class="fas fa-hdd ml-3"></i> <?php echo $size;
                endif; ?>
              </div>
            </div>
            <div class="ml-3">
              <a class="wpdm-download-link download-on-click btn btn-primary btn-sm" rel="nofollow" href="#" data-downloadurl="<?php echo site_url() ?>/download/on-boarding-process/?wpdmdl=4150&amp;refresh=621dc10a4f8f61646117130">Download</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php 
  } ?>
  
  <div class="allowed_post panel_heading" data-uid="<?php echo $uid ?>">
    <h4><?php echo $query1->found_posts; ?> Download(s) shared with you.</h4>
  </div>
  
  <?php
  ob_start();

  // The Loop
  if ( $query1->have_posts() ) :
    while ( $query1->have_posts() ) :
      $query1->the_post();
      $default_icon = site_url() . '/wp-content/plugins/download-manager/assets/file-type-icons/down2.png';
      $allowed_to = get_package_data(get_the_ID(), 'access');
      $size = get_package_data(get_the_ID(), 'package_size');
      $files = get_package_data(get_the_ID(), 'files');
      $icon = get_package_data(get_the_ID(), 'icon');
  
      // if (in_array($uroles, (array) $allowed_to)) :
        get_the_custom_wpdmpro_template($default_icon, $icon, $dname, $size, $files);
      // endif;
    endwhile; 
    wp_reset_postdata(); ?>
  
    <div class="wpdmc_pagination">
      <?php 
          echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query1->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Downloads', 'lavista' ) ),
            'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Downloads', 'lavista' ) ),
            'add_args'     => false,
            'add_fragment' => '',
          ) );
      ?>
    </div>
  
  <?php
  else : ?>
    <p><?php echo 'Sorry, no downloads matched your criteria. You need to be in group to access the downloads.' ?></p>

  <?php
  endif;

  return ob_get_clean();
  ob_end_clean();
}


function get_allowed_wpdm_posts_shortcode() {
  add_shortcode('wpdm_allowed_post', 'get_allowed_wpdm_posts');
}
add_action( 'init', 'get_allowed_wpdm_posts_shortcode' );


// $menu_items is an array containing current menu items
function my_custom_dashboard_menus($menu_items){ 
  // Removing edit profile menu
  unset($menu_items['download-history']);
  unset($menu_items['my-downloads']);
  return $menu_items;
}
add_filter('wpdm_user_dashboard_menu','my_custom_dashboard_menus', 10, 1);


function md_global_shortcode_include() {
  $current_user = wp_get_current_user();
  if (is_page('my-document-vault') && preg_match('/adb_page=user-categories/i', $_SERVER['REQUEST_URI'])) {
    echo '<div class="user__catergory">
      <div class="cat__btns">
        <button id="cat__advice-policies" class="active">advice policies</button>
        <button id="cat__licensee-policies">licensee policies</button>
        <button id="cat__licensee-templates">licensee templates</button>
        <button id="cat__other">other</button>
      </div>

      <div class="cat__tab" data-id="cat__advice-policies" style="display:block">';
          $args = array(
            'post_type' => 'wpdmpro',
            'post_status' => 'publish',
            'posts_per_page' => 999,
            'update_post_meta_cache' => false,
            'author' => $current_user->ID,
            'tax_query' => array(
              array(
                'taxonomy' => 'wpdmcategory',
                'field'    => 'slug',
                'terms'    => 'advice-policies',
              ),
            ),
          );
          $query = new WP_Query( $args );
          if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
              $query->the_post(); 
              echo do_shortcode('[wpdm_package id="'.get_the_ID().'" template="default_link_template_cloned_v1"]');
            } 
            wp_reset_postdata();
          } else { ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php 
          }
      echo '</div>

      <div class="cat__tab" data-id="cat__licensee-policies" style="display:none">';
        $args = array(
          'post_type' => 'wpdmpro',
          'post_status' => 'publish',
          'posts_per_page' => 999,
          'update_post_meta_cache' => false,
          'author' => $current_user->ID,
          'tax_query' => array(
            array(
              'taxonomy' => 'wpdmcategory',
              'field'    => 'slug',
              'terms'    => 'licensee-policies',
            ),
          ),
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
            $query->the_post(); 
            echo do_shortcode('[wpdm_package id="'.get_the_ID().'" template="default_link_template_cloned_v1"]');
          } 
          wp_reset_postdata();
        } else { ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php 
        }
      echo '</div>

      <div class="cat__tab" data-id="cat__licensee-templates" style="display:none">';
        $args = array(
          'post_type' => 'wpdmpro',
          'post_status' => 'publish',
          'posts_per_page' => 999,
          'update_post_meta_cache' => false,
          'author' => $current_user->ID,
          'tax_query' => array(
            array(
              'taxonomy' => 'wpdmcategory',
              'field'    => 'slug',
              'terms'    => 'licensee-templates',
            ),
          ),
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
            $query->the_post(); 
            echo do_shortcode('[wpdm_package id="'.get_the_ID().'" template="default_link_template_cloned_v1"]');
          } 
          wp_reset_postdata();
        } else { ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php 
        }
      echo '</div>

      <div class="cat__tab" data-id="cat__other" style="display:none">';
        $args = array(
          'post_type' => 'wpdmpro',
          'post_status' => 'publish',
          'posts_per_page' => 999,
          'update_post_meta_cache' => false,
          'author' => $current_user->ID,
          'tax_query' => array(
            array(
              'taxonomy' => 'wpdmcategory',
              'field'    => 'slug',
              'terms'    => 'other',
            ),
          ),
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
            $query->the_post(); 
            echo do_shortcode('[wpdm_package id="'.get_the_ID().'" template="default_link_template_cloned_v1"]');
          } 
          wp_reset_postdata();
        } else { ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php 
        }
      echo '</div>
    </div>';
  }
}


function md_user_group_wpdm_callback() {
  $user = wp_get_current_user();

  if (is_user_logged_in() && ($user->roles[0] == 'subscriber' || $user->roles[0] == 'administrator') && (is_page('my-document-vault') && preg_match('/adb_page=user-group/i', $_SERVER['REQUEST_URI']))) {
    echo '<div class="wpdmc">'.do_shortcode("[wpdm_allowed_post]").'</div>';
  }
}


function wpdm_author_dashboard_menus($menu_items){ 
  unset($menu_items['categories']);
  // unset($menu_items['Edit Profile']); // not working using this method. disabled via JS
  // unset($menu_items['settings']); // not working using this method. disabled via JS
  $menu_items['user-categories'] = array('name' => __('Category'), 'callback' => 'md_global_shortcode_include'); // naming is in loggedin-user.js
  $menu_items['user-group'] = array('name' => __('Group'), 'callback' => 'md_user_group_wpdm_callback'); // naming is in loggedin-user.js
  return $menu_items;
}
add_filter('wpdm_frontend','wpdm_author_dashboard_menus', 10, 1);


function be_dps_post_id_class( $classes, $post ) {
	$classes[] = 'post-' . get_the_ID();
	return $classes;
}
add_filter( 'display_posts_shortcode_post_class', 'be_dps_post_id_class', 10, 2 );


function term_admin_side() {
  if (preg_match('/term.php/i', $_SERVER['REQUEST_URI']) && preg_match('/taxonomy=group&tag_ID/i', $_SERVER['REQUEST_URI'])) {

    $terms = get_terms( array(
      'taxonomy'   => 'group', // Swap in your custom taxonomy name
      'hide_empty' => false, 
    )); 

    foreach($terms as $term) {

      $events_ex = array(
        'post_type' => 'wpdmpro',
        'post_status' => 'publish',
        'posts_per_page' => 999,
        'tax_query' => array(
          array(
            'taxonomy' => 'group',
            'field' =>    'slug',
            'terms' =>    $term->slug,
          ),
        ),
      );
      $events_ex = new WP_Query( $events_ex ); 

      echo '<ul data-term-name="'. $term->slug .'" class="admin_terms" style="display: none;">';
      if ( $events_ex->have_posts() ) {
        while ( $events_ex->have_posts() ) {
          $events_ex->the_post(); 

          $author_login = get_the_author_meta('user_login');
          $author_dname = get_the_author_meta('display_name');

          echo '<li data-term-author="'. $author_login .' ('. $author_dname .')"></li>';
        }
        wp_reset_postdata();
      } else { ?>
        <li><?php _e( 'Sorry, no posts matched your criteria.' ); ?></li>
        <?php 
      }
      echo '</ul>';
    }
  }
}
add_action('admin_head', 'term_admin_side');


function md_view_packages_option() {
  if ($GLOBALS['pagenow'] !== 'nav-menus.php' && ($GLOBALS['pagenow'] !== 'post.php' && $GLOBALS['typenow'] !== 'page') && ($GLOBALS['taxnow'] !== 'wpdmcategory' && $GLOBALS['typenow'] !== 'wpdmpro')) return;
  wp_enqueue_script( 'md-view-package-option', get_template_directory_uri() . '/assets/js/md-view-package.js', array(), '1.0', false );
}
add_action('admin_enqueue_scripts', 'md_view_packages_option');


add_filter( 'acf/validate_value/name=member_level', 'validate_user_member_level', 10, 4 );
function validate_user_member_level( $valid, $value, $field, $input ){

  // bail early if value is already invalid
  if ( $valid !== true ) { 
  return $valid; 
  }

  if ( empty($value) && ($input == "acf[field_624aa59f5319e]" )) {
      $valid = __( 'Please select a member level for this user' );
    }

  return $valid;

}

add_filter('get_post_status', function($post_status, $post) {
  if ($post->post_type == 'post' && $post_status == 'future') {
      return "publish";
  }
  return $post_status;
}, 10, 2);




function sk_entry_terms() {
	$terms = get_terms(array('taxonomy'  => 'wpdmcategory'));

	if ( ! empty( $terms ) ) {
    $array_terms = [];
    foreach ( $terms as $term ) {
      $array_terms[] = $term->slug;
    }
		return $array_terms;
	}
}


function filtered_wpdm_terms() {
  global $current_user; 

  $umeta = get_user_meta( $current_user->ID, 'member_level', true );
  $my_terms = get_terms(array('taxonomy'  => 'wpdmcategory'));
  $slg = sk_entry_terms();

  if (count($my_terms) > 0) {
    
    foreach ($my_terms as $titem) {
      $tmeta = get_term_meta( $titem->term_id, 'member_level', true );

      if (empty($tmeta)) continue;

      for ($i=0; $i < count($tmeta); $i++) {
        if (in_array($tmeta[$i], $umeta)) {
          if (($key = array_search("$titem->slug", $slg)) !== false) {
            unset($slg[$key]);
            break;
          }
        }
      }
    }
  }

  return $slg;
}


function member_level_term_shortcode() {
  if (!is_user_logged_in()) return;

  ob_start();

  $fterms = filtered_wpdm_terms();
  $imp_array = implode (",", $fterms);

  echo '<div id="advpack_filter">';
    echo do_shortcode('[wpdm_category id="'.$imp_array.'" orderby="title" order="desc" items_per_page="12" template="default_link_template_cloned_v1" paging=1 cols=1 colspad=1 colsphone=1 include_children=0 toolbar=1]');
  echo '</div>';

  $output = ob_get_clean();
  return $output;

}
add_shortcode('member_level_term', 'member_level_term_shortcode');



function member_level_term_admin_shortcode($ordrby) {
  if (!is_user_logged_in()) return;

  ob_start();
  
  $atts = shortcode_atts(array('orderby' => ''), $ordrby);
  $ob = $atts['orderby'];
  $fterms = filtered_wpdm_terms();
  $imp_array = implode (",", $fterms);

  echo do_shortcode('[wpdm_category id="'.$imp_array.'" orderby="'.$ob.'" order="desc" items_per_page="4" paging=0 cols=1 colspad=1 colsphone=1 include_children=0 toolbar=0]');

  $output = ob_get_clean();
  return $output;

}
add_shortcode('member_level_term_admin', 'member_level_term_admin_shortcode');


function md_enewsletter_archive_shortcode() {
  ob_start();

  $args = array(
    'post_type'       => 'post',
    'post_status'     => array( 'publish', 'future' ),
    'posts_per_page'  => -1,
    'tax_query'       => array(
      array(
        'taxonomy'    => 'category',
        'field'       => 'slug',
        'terms'       => 'lavista-news',
      ),
    ),
  );
  $enews = new WP_Query( $args ); ?>
  
  <ul class="enewsletter_list">
  <?php
    if ( $enews->have_posts() ) :
      while ( $enews->have_posts() ) : $enews->the_post(); 
      $enews_link = get_field('new_lavista_post_permalink', get_the_ID());
      ?>
      <li class="enewsletter_item"><a class="enewsletter_link" href="<?php echo empty($enews_link) ? get_the_permalink() : $enews_link; ?>"><?php echo get_the_title(); ?></a></li>
      <?php
      endwhile; 
      wp_reset_postdata();
    else : ?>
      <li>No eNewsletter found.</li>
      <?php
    endif;
  ?>
  </ul>
  <?php

  $output = ob_get_clean();
  return $output;
  ob_end_clean();
}
add_shortcode('enewsletter_archive', 'md_enewsletter_archive_shortcode');


function lavista_events_shortcode() { 
  ob_start();

  $today = date("Y-m-d H:i:s"); 
  ?>

  <div class="event_tab" id="event_tab">
    <button class="tab_links activ" data-events="upcoming">Upcoming Events</button>
    <button class="tab_links" data-events="past">Past Events</button>
  </div>

  <div class="tab_content" id="event_upcoming">
    <?php
    $args1 = array(
      'post_type'       => 'event',
      'post_status'     => 'publish',
      'posts_per_page'  => -1,
      'meta_key'        => 'event_dates_event_start',
      'orderby'         => 'meta_value',
      'order'           => 'ASC',
      'meta_query'      => array(
        'relation' => 'OR',
        array(
          'key'     => 'event_dates_event_start',
          'compare' => '>=',
          'value'   => $today,
        ),
        array(
          'key'     => 'event_dates_event_end',
          'compare' => '>=',
          'value'   => $today,
        ),
      )
    );

    $query1 = new WP_Query( $args1 );

    if ($query1->have_posts()) : 
      get_template_part('template-parts/filter/event', 'filter', array('query' => $query1));
      ?>
      <div class="post_items" data-event-filter data-post-type="event" data-max-num="<?php echo $query1->max_num_pages ?>" data-page-num="1" data-found-post="<?php echo $query1->found_posts ?>" data-per-page="<?php echo $query1->query_vars['posts_per_page'] ?>">
      
        <?php
        while ( $query1->have_posts() ) : $query1->the_post();
          get_template_part('template-parts/content/event', 'content');
        endwhile;
        wp_reset_postdata(); ?>

      </div>

      <div class="ajax_preloader" style="display: none;">
        <div><img height="24" width="24" src="<?php echo admin_url().'/images/spinner-2x.gif' ?>" alt="spinner"/>
        </div><div><span>Loading....</span></div>
      </div>

      <script>
        const query1 = <?php echo json_encode($query1) ?>;
        console.log(query1)
      </script>
      <?php
    else: ?>
      <p><?php _e( 'No upcoming events.' ); ?></p>
      <?php
    endif; ?>
  </div>

  <div class="tab_content" id="event_past" style="display: none;">
    <?php
    $args2 = array(
      'post_type'       => 'event',
      'post_status'     => 'publish',
      'posts_per_page'  => -1,
      'meta_key'        => 'event_dates_event_start',
      'orderby'         => 'meta_value',
      'order'           => 'DESC',
      'meta_query'      => array(
        'relation' => 'AND',
        array(
          'key'     => 'event_dates_event_start',
          'compare' => '<',
          'value'   => $today,
        ),
        array(
          'key'     => 'event_dates_event_end',
          'compare' => '<',
          'value'   => $today,
        ),
      )
    );

    $query2 = new WP_Query( $args2 );

    if ($query2->have_posts()) : 
      get_template_part('template-parts/filter/event', 'filter', array('query' => $query2));
      ?>
      <div class="post_items" data-event-filter data-post-type="event" data-max-num="<?php echo $query2->max_num_pages ?>" data-page-num="1" data-found-post="<?php echo $query2->found_posts ?>" data-per-page="<?php echo $query2->query_vars['posts_per_page'] ?>">
      
        <?php
        while ( $query2->have_posts() ) : $query2->the_post();
          get_template_part('template-parts/content/event', 'content');
        endwhile;
        wp_reset_postdata(); ?>

      </div>

      <div class="ajax_preloader" style="display: none;">
        <div><img height="24" width="24" src="<?php echo admin_url().'/images/spinner-2x.gif' ?>" alt="spinner"/>
        </div><div><span>Loading....</span></div>
      </div>

      <script>
        console.log(<?php echo json_encode($query2) ?>)
      </script>
      <?php
    else: ?>
      <p><?php _e( 'No past events.' ); ?></p>
      <?php
    endif; ?>
  </div>
  <?php
  return ob_get_clean();
}
add_shortcode('lavista_events', 'lavista_events_shortcode');


function lavista_filter_event_ajax_handler() {
  $event_type = $_POST['event_type'];
  $event_code = $_POST['event_code'];
  // $event_state = $_POST['event_state'];
  $reset = $_POST['reset'];
  $today = date("Y-m-d H:i:s"); 

  if (isset($event_type) || isset($event_code) || isset($reset)) :
    $args = array(
      'post_type'       => 'event',
      'post_status'     => 'publish',
      'posts_per_page'  => -1,
      'meta_key'        => 'event_dates_event_start',
      'orderby'         => 'meta_value',
    );

    if (isset($reset) && $reset && $event_code === 'event_upcoming') :
      $args['order'] = 'ASC';
      $args['meta_query'] = array(
        'relation' => 'OR',
        array(
          'key'     => 'event_dates_event_start',
          'compare' => '>=',
          'value'   => $today,
        ),
        array(
          'key'     => 'event_dates_event_end',
          'compare' => '>=',
          'value'   => $today,
        ),
      );

    elseif (isset($reset) && $reset && $event_code === 'event_past') :
      $args['order'] = 'DESC';
      $args['meta_query'] = array(
        'relation' => 'AND',
        array(
          'key'     => 'event_dates_event_start',
          'compare' => '<',
          'value'   => $today,
        ),
        array(
          'key'     => 'event_dates_event_end',
          'compare' => '<',
          'value'   => $today,
        ),
      );
    
    else :
      if ($event_code === 'event_upcoming') :
        $args['order'] = 'ASC';
        $args['meta_query'] = array(
          'relation' => 'AND',
          array(
            'key'     => 'event_type',
            'compare' => '=',
            'value'   => $event_type,
          ),
          array(
            'relation' => 'OR',
            array(
              'key'     => 'event_dates_event_start',
              'compare' => '>=',
              'value'   => $today,
            ),
            array(
              'key'     => 'event_dates_event_end',
              'compare' => '>=',
              'value'   => $today,
            ),
          )
        );

        // if (isset($event_state)) :
        //   $args['meta_query'][] = array(
        //     'key'     => 'event_location',
        //     'compare' => 'LIKE',
        //     'value'   => $event_state.'";s:11:',
        //   );
        // endif;

      else :
        $args['order'] = 'DESC';
        $args['meta_query'] = array(
          'relation' => 'AND',
          array(
            'key'     => 'event_type',
            'compare' => '=',
            'value'   => $event_type,
          ),
          array(
            'relation' => 'AND',
            array(
              'key'     => 'event_dates_event_start',
              'compare' => '<',
              'value'   => $today,
            ),
            array(
              'key'     => 'event_dates_event_end',
              'compare' => '<',
              'value'   => $today,
            ),
          )
        );

        // if (isset($event_state)) :
        //   $args['meta_query'][] = array(
        //     'key'     => 'event_location',
        //     'compare' => 'LIKE',
        //     'value'   => $event_state.'";s:11:',
        //   );
        // endif;
      endif;
    endif;

    $query = new WP_Query( $args );

    ob_start();

    if ($query->have_posts()) :
      while ($query->have_posts()) : $query->the_post();
        get_template_part('template-parts/content/event', 'content');
      endwhile;
      wp_reset_postdata();
    else: ?>
      <p><?php echo sprintf( 'No %s events', $event_code === 'event_upcoming.' ? 'upcoming' : 'past' ); ?></p>
      <?php
    endif;

    $results = ob_get_contents();
    ob_get_clean();

    $data = array(
      'results'   => $results,
      'args'      => $args,
      'query'     => $query,
      'request'   => $_POST
    );
    
    wp_send_json_success( $data );
  else :
    wp_send_json_error('Invalid input!');
  endif;
}
add_action('wp_ajax_filter_event', 'lavista_filter_event_ajax_handler'); 
add_action('wp_ajax_nopriv_filter_event', 'lavista_filter_event_ajax_handler'); 



function lavista_add_login_body_class($classes, $action) {
  $classes[] = 'lavista';
  return $classes;
}
add_filter( 'login_body_class', 'lavista_add_login_body_class', 10, 2 );

function lavista_replace_login_url($url) {
  $url = home_url();
  return $url;
}
add_filter( 'login_headerurl', 'lavista_replace_login_url' );

function lavista_login_logo_url_title($heading) {
  $heading = get_bloginfo( 'name' );
  return $heading;
}
add_filter( 'login_headertext', 'lavista_login_logo_url_title' );

function lavista_login_scripts() { 
  $custom_logo_id = get_theme_mod( 'custom_logo' );
  $favicon = get_site_icon_url(250, esc_url(admin_url('/images/wordpress-logo.svg')));
  echo '<script>console.log('.json_encode($favicon).')</script>';
  $image = $custom_logo_id ? wp_get_attachment_image_src( $custom_logo_id , 'full' ) : false; ?>

  <style type="text/css">
      .lavista.login {
          background-color: #1b3837;
      }
      .lavista #login h1 a, .lavista.login h1 a {
          background-image: url(<?php echo has_custom_logo() ? $image[0] : $favicon ?>);
          height:<?php echo has_custom_logo() ? $image[2] :'50' ?>px;
          width: <?php echo has_custom_logo() ? $image[1] :'50' ?>px;
          background-size: contain;
      }
      .lavista.login #backtoblog a, .lavista.login #nav a,
      .lavista.login #backtoblog a:hover, .lavista.login #nav a:hover {
          color: white;
      }
      .lavista.login #backtoblog a:hover, .lavista.login #nav a:hover {
          text-decoration: underline;
      }
      .lavista.wp-core-ui .button-primary {
        background-color: #12606b;
        border-color: #12606b;
        transition: all .15s ease-in-out;
      }
      .lavista.wp-core-ui .button-primary:hover {
        background-color: #1b3837;
        border-color: #1b3837;
      }
  </style>
  <?php 
}
add_action( 'login_enqueue_scripts', 'lavista_login_scripts' );


add_action( 'init', 'blockusers_init' );
function blockusers_init() {
    if ( is_user_logged_in() && is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url('/admin') );
        exit;
    }
}


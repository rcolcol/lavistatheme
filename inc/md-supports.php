<?php
/**
 * Lavista supports
 *
 *
 * @package WordPress
 */

/**
 * ACF REGISTERING GOOGLE API KEY
 */
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
function my_acf_google_map_api( $args ) {
	$args['key'] = 'AIzaSyBVYB_M-_7PLPop9ydYFVosVBQpx304YCo';
	return $args;
}


add_action( 'init', 'md_post_type_event_init', 0 );
add_action( 'init', 'md_group_taxonomies', 0 );
add_action( 'init', 'md_event_category_taxonomies', 0 );

function md_post_type_event_init() {
	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name' ),
		'menu_name'             => __( 'Events' ),
		'name_admin_bar'        => __( 'Events' ),
		'archives'              => __( 'Event Archives' ),
		'parent_item_colon'     => __( 'Parent Events:' ),
		'all_items'             => __( 'All Events' ),
		'add_new_item'          => __( 'Add New Event' ),
		'add_new'               => __( 'Add New' ),
		'new_item'              => __( 'New Event' ),
		'edit_item'             => __( 'Edit Event' ),
		'view_item'             => __( 'View Event' ),
		'search_items'          => __( 'Search Events' ),
		'not_found'             => __( 'Not Events found.' ),
		'not_found_in_trash'    => __( 'Not Events found in Trash.' ),
		'featured_image'        => __( 'Event Featured Image' ),
		'set_featured_image'    => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image'    => __( 'Use as featured image' ),
		'insert_into_item'      => __( 'Insert into Event' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event' ),
		'items_list'            => __( 'Events list' ),
		'items_list_navigation' => __( 'Events list navigation' ),
		'filter_items_list'     => __( 'Filter Events list' ),
	);
	$args = array(
		'label'                 => __( 'Events' ),
		'description'           => __( 'Events custom post type.' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'revisions', 'thumbnail' ),
		'taxonomies'            => array( 'event_cat' ),
		'rewrite'               => array( 'slug' => 'events' ),
		'hierarchical'          => false,
		'query_var'             => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-calendar',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'has_archive'           => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'event', $args );
}


function md_group_taxonomies() {
	$labels = array(
		'name'              => _x( 'Groups', 'taxonomy general name' ),
		'singular_name'     => _x( 'Group', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Groups' ),
		'all_items'         => __( 'All Groups' ),
		'parent_item'       => __( 'Parent Group' ),
		'parent_item_colon' => __( 'Parent Group:' ),
		'edit_item'         => __( 'Edit Group' ),
		'update_item'       => __( 'Update Group' ),
		'add_new_item'      => __( 'Add New Group' ),
		'new_item_name'     => __( 'New Group Name' ),
		'menu_name'         => __( 'Groups' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'group' ),
	);
	register_taxonomy( 'group', array( 'wpdmpro' ), $args );
}


function md_event_category_taxonomies() {
	$labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'event_cat' ),
	);
	register_taxonomy( 'event_cat', array( 'event' ), $args );
}

// get_excerpt($post, $description, $count, $btntxt, $target);
function get_excerpt($post, $description, $count, $more='', $target='_self') {
    $permalink = get_permalink($post->ID);
    $more = $more != '' ? '<div class="btn_wrapper"><a class="post_more btn accent" href="'.$permalink.'" target="'.$target.'">'.$more.'</a></div>' : '';
    $strip_content = strip_tags($description);
    $excerpt = strlen($strip_content) > 0 ? substr($strip_content, 0, $count) : '';
    $excerpt = $excerpt != '' ? '<div class="post_excerpt"><p>'.$excerpt : '';
    $excerpt = $count < strlen($strip_content) && $excerpt != '' ? "$excerpt....</p></div>" : "$excerpt</p></div>";
    $excerpt = $excerpt.$more;
    return $excerpt;
}


function lavista_svg_icon($icon, $size) {
	return '<svg class="feather" width="'.$size.'" height="'.$size.'"><use href="'.get_template_directory_uri() . '/assets/feather-icons/feather-sprite.svg#'.$icon.'"/></svg>';
}
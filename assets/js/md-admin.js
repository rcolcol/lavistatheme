(function ($) {

  const wpdmSettings = document.querySelector('#wpdm-settings');
  const wpdmPostEdit = 'action=edit';
  const wpdmPost = 'post.php?post';
  const wpdmCreatePost = 'post-new.php?post_type=wpdmpro';
  const editTagsGroup = 'edit-tags.php?taxonomy=group';
  const addTag = document.querySelector('#addtag');

  // user profile page
  const removePaypalField = () => {
    if (location.pathname.match(/\/wp-admin\/user-edit\.php$/i) || location.pathname.match(/\/wp-admin\/profile\.php$/i)) {
      console.dir(location)
      const yourProfile = document.querySelector('#your-profile');

      if (yourProfile !== null && yourProfile !== undefined) {
        const paypalEmail = yourProfile['__wpdm_public_profile[paypal]'];

        if (paypalEmail !== null && paypalEmail !== undefined) {
          const paypalParent = paypalEmail.closest('.panel');

          paypalParent.remove();
          setTimeout(() => {
            yourProfile.classList.add('profile_form_loaded');
          }, 500);
        }
        
        yourProfile.classList.add('profile_form_loaded');
      } 
    }
  }

  // wpdmpro post.php
  const fnWPDMPost = () => {
    if ( (location.href.indexOf(`${wpdmCreatePost}`) > -1 || (location.href.indexOf(`${wpdmPost}`) > -1 && location.href.indexOf(`${wpdmPostEdit}`) > -1)) && wpdmSettings ) {
      const rowSelectMembers = wpdmSettings.querySelector('#select_members_row');
      const divGroup = document.querySelector('#groupdiv');

      if (rowSelectMembers && divGroup) {
        rowSelectMembers.remove()
        divGroup.remove()
      }
    }
  }

  // edit-tags.php
  const fnHideAcfTax = () => {
    if (addTag && location.href.indexOf(`${editTagsGroup}`) > -1) {
      const acfFields = addTag.querySelector('#acf-term-fields');
      acfFields.remove()
    }
  }

  document.addEventListener('DOMContentLoaded', function() {
    removePaypalField();
    fnWPDMPost();
    fnHideAcfTax()
  });

  window.addEventListener('load', function() {
    fnWPDMPost();
  })

})(jQuery);
(function ($) {

  const fnCreatePostFrontEndAjax = () => {
    const pageURL = location.pathname;
    const editPackage = `${pageURL}?adb_page=edit-package`;

    if (location.href.indexOf(`${editPackage}`) > -1) {
      const wpdmproGroupTerm = document.querySelector('#wpdmpro_group_term');
      const publishBtn = document.querySelector('#publish');

      if (wpdmproGroupTerm != null && publishBtn) {
        const termLists = wpdmproGroupTerm.querySelectorAll('.wpdmpro_group_term_list');

        if (termLists.length > 0) {
          const filteredTermList = Array.from(termLists).find(term => {
            const currentUser = document.querySelector('#auth__logout');
            const currentUserID = currentUser.getAttribute('data-uid')

            if (term.innerHTML.indexOf(`data-member-id="${currentUserID}"`) > -1) {
              return term;
            }
          });

          function fnTermID(term) {
            if (term) return term.getAttribute('data-term-id');
          }

          function fnTermName(term) {
            if (term) return term.getAttribute('data-term-name');
          }

          function fnTermGroup(term) {
            if (term) {
              const members = term.querySelectorAll('.group_member');
              // userNames declared in functions
              const arrUserNames = Array.from(userNames);
              let memberArr = new Array();

              function fnMemberListNames(el) {
                Array.from(members).forEach(member => {
                  const memberID = member.getAttribute('data-member-id');

                  const filteredUserNames = arrUserNames.filter(user => {
                    if (String(user).indexOf(`${parseInt(memberID)}`) > -1) return user;
                  });

                  filteredUserNames.forEach(user => {
                    const userOpenIndex = user.indexOf('(');
                    const userInitial = user.slice(0, parseInt(userOpenIndex));
                    const userSpaceIndex = userInitial.lastIndexOf(' ');
                    const userLoginValue = userInitial.slice(0, parseInt(userSpaceIndex));

                    if (!el.includes(userLoginValue)) {
                      el.push(userLoginValue);
                    }
                  })
                })
                return el;
              }
              fnMemberListNames(memberArr);
              return memberArr;

            } else {
              console.log('The login user is not currently not belong to a group');
            }
          }

          const wpdmPF = document.querySelector('#wpdm-pf');
          const postID = wpdmPF.querySelector('#id');
          const postIDValue = postID.value;
          let clicked = false;

          publishBtn.addEventListener('click', function () {

            if (!clicked) {
              clicked = true

              if (postIDValue && fnTermGroup(filteredTermList) != undefined && fnTermName(filteredTermList) != undefined && fnTermID(filteredTermList) != undefined) {
                console.log(postIDValue)
                console.log(fnTermGroup(filteredTermList))
                console.log(fnTermName(filteredTermList))
                console.log(fnTermID(filteredTermList))
              }

              $.ajax({
                url: ajaxcalls.ajaxurl,
                type: 'POST',
                data: {
                  action: 'ajax_calls',
                  post_id: postIDValue,
                  user_name: fnTermGroup(filteredTermList),
                  term_name: fnTermName(filteredTermList),
                  term_id: fnTermID(filteredTermList),
                },
                beforeSend: function (html) {},
                success: function (html) {},
              })
            }
          });
        }
      }
    }
  }

  document.addEventListener('DOMContentLoaded', fnCreatePostFrontEndAjax);

})(jQuery);
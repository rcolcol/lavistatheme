;(function () {
	"use strict"

	const ajaxurl = mainParams.ajaxurl
	const nonce = mainParams.nonce

	const handleWPRequest = async (url = "", obj = {}) => {
		const postRequest = new Request(url, {
			method: "POST",
			body: obj,
		})

		const response = await fetch(postRequest)
		return response.json()
	}

	window.ajaxurl = ajaxurl
	window.nonce = nonce
	window.handleWPRequest = handleWPRequest
})(window)
;(function ($) {
	"use strict"

	// <!-- ACF GOOGLE MAP INITIALIZATION -->
	const handleGoogleMapInit = () => {
		function initMap($el) {
			// Find marker elements within map.
			var $markers = $el.find(".marker")

			// Create gerenic map.
			var mapArgs = {
				zoom: $el.data("zoom") || 4,
				center: google.maps.LatLng(0, 0),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			}
			var map = new google.maps.Map($el[0], mapArgs)

			// Add markers.
			map.markers = []

			// Collect all infowindow
			let infoWindows = []

			$markers.each(function () {
				initMarker($(this), map, infoWindows)
			})

			// Center map based on markers.
			centerMap(map)

			// Return map instance.
			return map
		}

		function initMarker($marker, map, panels) {
			// Get position from marker.
			var lat = $marker.data("lat")
			var lng = $marker.data("lng")
			var latLng = {
				lat: parseFloat(lat),
				lng: parseFloat(lng),
			}

			// Create marker instance.
			var marker = new google.maps.Marker({
				position: latLng,
				map: map,
			})
			// console.log(marker.position)

			// Append to reference for later use.
			map.markers.push(marker)

			// If marker contains HTML, add it to an infoWindow.
			if ($marker.html()) {
				// Create info window.
				var infowindow = new google.maps.InfoWindow({
					content: $marker.html(),
				})

				panels.push(infowindow)

				// Show info window when marker is clicked.
				google.maps.event.addListener(marker, "click", function () {
					panels.forEach((panel) => panel.close())
					infowindow.open(map, marker)
					map.zoom = 14

					const activPin = map.markers.find((pin) => pin === marker)
					var bounds = new google.maps.LatLngBounds()

					bounds.extend({
						lat: activPin.position.lat(),
						lng: activPin.position.lng(),
					})

					map.setCenter(bounds.getCenter())
				})

				google.maps.event.addListener(map, "click", function () {
					infowindow.close()
				})
			}
		}

		function centerMap(map) {
			// Create map boundaries from all map markers.
			var bounds = new google.maps.LatLngBounds()
			map.markers.forEach(function (marker) {
				bounds.extend({
					lat: marker.position.lat(),
					lng: marker.position.lng(),
				})
			})

			// Case: Single marker.
			if (map.markers.length == 1) {
				map.setCenter(bounds.getCenter())
				map.setZoom(12)

				// Case: Multiple markers.
			} else {
				map.fitBounds(bounds)
				map.setZoom(4)
			}
		}

		// Render maps on page load.
		$(".acf-map").each(function () {
			initMap($(this))
		})
	}

	document.addEventListener("DOMContentLoaded", handleGoogleMapInit)

	window.mapInit = handleGoogleMapInit
})(jQuery, window)
;(function ($) {
	"use strict"

	document.addEventListener("DOMContentLoaded", function () {
		const body = document.body
		const navElement = document.querySelector(".navbar")

		if (navElement) {
			handleElementHeight(navElement, body)
		}

		function getElementHeight(element) {
			const height = element.offsetHeight
			// console.log("Element height:", height)
			return height
		}

		function handleElementHeight(nav, body) {
			const navHeight = getElementHeight(nav)
			body.style.paddingTop = `${navHeight}px`

			window.addEventListener("scroll", () => {
				const navHeight = getElementHeight(nav)

				body.style.paddingTop = `${navHeight}px`
			})
		}

		const backtoTop = () => {
			const topBtn = document.querySelector("#backto_top")
			window.onscroll = function () {
				if (window.pageYOffset > 768) {
					setTimeout(() => {
						topBtn.classList.add("visible")
					}, 200)
				} else {
					setTimeout(() => {
						topBtn.classList.remove("visible")
					}, 200)
				}
			}

			topBtn.addEventListener("click", (e) => {
				e.preventDefault()
				const htmlBody = $("html, body")
				htmlBody.animate(
					{
						scrollTop: 0,
					},
					500
				)
			})
		}
		backtoTop()

		const lostPasswordPage = () => {
			const resetForm = document.querySelector("#resetPassword")
			if (body.classList.contains("actionlostpassword") && resetForm) {
				const resetFormLinks = resetForm.querySelectorAll("a")
				resetFormLinks.forEach((link) => {
					if (link.getAttribute("href").includes("action=register")) {
						link.remove()
					}
				})
				setTimeout(() => {
					resetForm.classList.add("reset_form_loaded")
				}, 500)
			}
			if (resetForm) {
				resetForm.classList.add("reset_form_loaded")
			}
		}
		lostPasswordPage()

		const navBarLinks = () => {
			const navBar = document.querySelector(".navbar")
			if (!body.classList.contains("home") && navBar) {
				navBar.setAttribute("style", "display: block;")
				setTimeout(() => {
					const lists = navBar.querySelectorAll("#primary li")
					lists.forEach((list) => {
						if (list.classList.contains("home_links")) {
							const menuItem = list.querySelector("a")
							const link = () => menuItem.getAttribute("href")
							menuItem.setAttribute("href", "/" + link())
						}
					})
				}, 250)
			}
		}
		navBarLinks()

		const eventTab = document.querySelector("#event_tab")
		const dataPostFilters = [...document.querySelectorAll("[data-post-filter]")]

		handleDataPostFilter(dataPostFilters)
		handleEventTab(eventTab)

		function handleDataPostFilter(filters) {
			if (filters.length === 0) return

			for (const filter of filters) {
				const eventType = filter.querySelector('[name="event_type"]')
				const eventFilter = filter.querySelector('[name="filter"]')
				const eventReset = filter.querySelector('[name="reset"]')

				// eventType.addEventListener('change', handleSelectEventType)
				eventFilter.addEventListener("click", handleClickFilter)
				eventReset.addEventListener("click", handleClickFilter)
			}

			// function handleSelectEventType() {
			//   const eventLocation = this.nextElementSibling

			//   if (this.value === 'in-person')
			//     eventLocation.style.display = 'block'
			//   else
			//     eventLocation.style.display = 'none'
			// }

			function handleClickFilter(e) {
				e.preventDefault()

				const postFilter = this.closest(".post_filter")
				const postItems = postFilter.nextElementSibling
				const preloader = postFilter.nextElementSibling.nextElementSibling
				const eventType = this.closest(".post_filter").querySelector(
					'[name="event_type"]'
				)
				// const eventState = this.closest('.post_filter').querySelector('[name="event_location"]')
				const eventCode = this.closest(".tab_content").id
				let evtTypeTxt = eventType.options[eventType.selectedIndex].text
				// let evtStateTxt = eventState.options[eventState.selectedIndex].text

				if (eventType.value == 0) {
					if (this.name === "reset") return

					alert("Select event type to filter events.")
					return
				}

				preloader.style.display = "flex"
				postItems.classList.add("fetching")

				let formData = new FormData()
				formData.append("action", "filter_event")
				formData.append("nonce", nonce)

				if (this.name === "filter") {
					formData.append("reset", 0)
					formData.append("event_type", evtTypeTxt)
					formData.append("event_code", eventCode)

					// if (eventType.value === 'in-person' && eventState.value != 0)
					// formData.append('event_state', evtStateTxt)
				} else if (this.name === "reset") {
					formData.append("reset", 1)
					formData.append("event_code", eventCode)
				}

				handleWPRequest(ajaxurl, formData)
					.then((data) => {
						console.log(data)

						if (this.name === "reset") {
							eventType.value = 0
							// eventState.value = 0
							// eventState.style.display = 'none'
						}
						postItems.dataset.foundPost = data.data.query.found_posts
						postItems.innerHTML = data.data.results
						preloader.style.display = "none"
						postItems.classList.remove("fetching")
					})
					.catch((error) => {
						console.warn(error)
					})
			}
		}

		function handleEventTab(tab) {
			if (tab == undefined) return

			const tabLinks = [...tab.querySelectorAll(".tab_links")]

			for (const link of tabLinks) {
				link.addEventListener("click", handleClickLinkTab)
			}

			function handleClickLinkTab(e) {
				e.preventDefault()

				const activTabContent = document.querySelector(
					`#event_${this.dataset.events}`
				)
				const tabContents = [...document.querySelectorAll(".tab_content")]
				const inactivLink = tabLinks.find((link) => link !== this)
				const inactivTab = tabContents.find((tab) => tab !== activTabContent)

				inactivLink.classList.remove("activ")
				this.classList.add("activ")

				inactivTab.style.display = "none"
				activTabContent.style.display = "block"
			}
		}
	})
})(jQuery)

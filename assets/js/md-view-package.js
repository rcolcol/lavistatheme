(function() {
  document.addEventListener('DOMContentLoaded', () => {
    const menuOptions = [...document.querySelectorAll('[data-name="member_level"]')]

    if (menuOptions.length > 0) {
      menuOptions.forEach(option => {
        const item = option.querySelector('[value="View all packages(Internal only)"]')

        if (item != null) {
          item.remove();
        }
      })
    }
  })
})()
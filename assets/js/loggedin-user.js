(function ($) {
  "use strict";

  // edit user front-end form
  const fnEditProfileForm = () => {
    const pageURL = location.pathname;
    const editProfile = `${pageURL}?udb_page=edit-profile`;

    if (location.href.indexOf(`${editProfile}`) > -1) {
      const editUserForm = document.querySelector("#edit_profile");

      if (editUserForm) {
        const paypalParent = editUserForm["payment_account"];
        const paypalEmail = paypalParent.closest(".form-group");
        const emailInput = editUserForm.querySelector("#email");
        const cards = editUserForm.querySelectorAll(".card");

        if (paypalEmail) paypalEmail.remove();

        if (emailInput) emailInput.setAttribute("readonly", "readonly");

        cards.forEach((card) => {
          if (card.innerHTML.includes("Payment Settings")) card.remove();
        });

        setTimeout(() => {
          editUserForm.classList.add("edit_form_loaded");
        }, 500);
      }
    }
  };

  // author dashboard - vault page
  const fnAuthorDashboard = (body) => {
    if (body.classList.contains("page-id-2707")) {
      const dashboard = document.querySelector(".author-dashbboard");
      const editAuthorLink = "?adb_page=edit-profile";
      const editSettingLink = "?adb_page=settings";
      const authorCategoryLink = "?adb_page=user-categories";
      const authorGroupLink = "?adb_page=user-group";
      const allDocsLink = "?adb_page=manage-packs";
      const sbLinks = dashboard.querySelectorAll("#wpdm-dashboard-sidebar a");
      const userCategory = document.querySelector(".user__catergory");
      const wpdmPF = document.querySelector("#wpdm-pf");
      const pathNameURL = location.pathname;
      const editPackageURL = `${pathNameURL}?adb_page=edit-package`;

      if (
        location.href.indexOf(`${editAuthorLink}`) > -1 ||
        location.href.indexOf(`${editSettingLink}`) > -1
      ) {
        location.replace("/admin");
      }

      if (sbLinks.length > 0) {
        sbLinks.forEach((sb) => {
          if (
            sb.getAttribute("href").indexOf(editAuthorLink) > -1 ||
            sb.getAttribute("href").indexOf(editSettingLink) > -1
          ) {
            sb.remove();
          }

          if (sb.getAttribute("href").includes(`${authorCategoryLink}`)) {
            sb.innerHTML =
              "<i class='fa fa-th-large mr-3' aria-hidden='true'></i>Categories";
          } else if (sb.getAttribute("href").includes(`${authorGroupLink}`)) {
            sb.innerHTML =
              "<i class='fa fa-users mr-3' aria-hidden='true'></i>Group Documents";
          } else if (sb.getAttribute("href").includes(`${allDocsLink}`)) {
            sb.innerHTML =
              "<i class='far fa-arrow-alt-circle-down mr-3'></i>My Documents";
          } else {
            // do nothing
          }
        });
      }

      if (userCategory) {
        const btns = userCategory.querySelectorAll(".cat__btns button");

        btns.forEach((btn) => {
          btn.addEventListener("click", function () {
            const btnID = this.getAttribute("id");
            const dataID = userCategory.querySelectorAll(".cat__tab");
            const tabDataID = userCategory.querySelector(`[data-id=${btnID}]`);

            if (btnID == btn.getAttribute("id")) {
              dataID.forEach((tab) => (tab.style.display = "none"));
              tabDataID.style.display = "block";
              btns.forEach((elem) => elem.classList.remove("active"));
              this.classList.add("active");
            }
          });
        });
      }

      // wpdmpro front-end add new post
      if (wpdmPF && location.href.indexOf(`${editPackageURL}`) > -1) {
        const cardHeader = wpdmPF.querySelector(".card-header");
        const featureImage = wpdmPF.querySelector("#featured-image-section");
        const addPreview = wpdmPF.querySelector("#additional-previews-section");
        const categorySectionSrc = wpdmPF.querySelector(
          "#categories-section #cat_src"
        );
        const tagSection = wpdmPF.querySelector("#tags-section");
        const packageSetting = wpdmPF.querySelector(
          "#package-settings-section"
        );
        const cardDefaults = wpdmPF.querySelectorAll(".card");

        packageSetting.setAttribute(
          "style",
          "margin: 0; padding:0; overflow:hidden; height:0; width:0;"
        );

        if (
          cardHeader &&
          cardHeader.textContent.includes("Create New Package")
        ) {
          cardHeader.textContent = "Create New Document";
        }

        if (featureImage && addPreview && categorySectionSrc && tagSection) {
          featureImage.remove();
          addPreview.remove();
          categorySectionSrc.remove();
          tagSection.remove();
        }

        if (cardDefaults.length > 0) {
          const foundAuthorCard = Array.from(cardDefaults).find((card) => {
            if (card.querySelector(".card-header").textContent === "Author") {
              return card;
            }
          });
          if (foundAuthorCard) {
            foundAuthorCard.remove();
          }
        }
      }
    }
  };

  // iframes
  const fnIIIframe = () => {
    const iframes = document.querySelectorAll(
      "#products_iframe, #cpd_iframe, #selProducts_iframe, #air_iframe"
    );
    const authName = document.querySelector("#auth__logout");
    const mainContent = document.querySelector(".main_content");

    if (iframes.length > 0 && authName && mainContent) {
      let dataName = authName.getAttribute("data-name");
      dataName = dataName.replace(/\s+/g, "");
      //    dataName = 'test1'

      async function iframeToken() {
        const response = await fetch(
          `https://informedinvestor.com.au/api/tools/get-token/Clearview_Matrix/${dataName}/bf9df678dbe024c1b9717fcb37641d4d`
        );
        const data = await response.json();
        return data;
      }

      iframeToken()
        .then((data) => {
          console.log(data);
          const token = data.token.token;
          const iframeLink = "https://informedinvestor.com.au/digital-tools/";
          const iframeLinkWithToken = `${iframeLink}${token}`;
          handleIframeSource(iframeLinkWithToken);
        })
        .catch((error) => {
          console.warn(error);
        });

      function handleIframeSource(link) {
        iframes.forEach((iframe) => {
          switch (iframe.id) {
            case "products_iframe":
              iframe.src = link;
              break;
            case "cpd_iframe":
              iframe.src = link;
              break;
            case "selProducts_iframe":
              iframe.src = link;
              break;
            case "air_iframe":
              iframe.src = link;
              break;
            default:
              console.log("No iframe loaded");
          }
        });
      }

      // $.get(
      //   `https://informedinvestor.com.au/api/tools/get-token/Clearview_Matrix/${dataName}/bf9df678dbe024c1b9717fcb37641d4d`,
      //   function (data) {
      //     const token = data.token.token;
      //     const iframeLink = "https://informedinvestor.com.au/digital-tools/";

      //     const iframeSource = (item) => {
      //       item.src = `${iframeLink}${token}`;
      //     };

      //     iframes.forEach((iframe) => {
      //       switch (iframe.id) {
      //         case "products_iframe":
      //           iframeSource(iframe);
      //           break;
      //         case "cpd_iframe":
      //           iframeSource(iframe);
      //           break;
      //         case "selProducts_iframe":
      //           iframeSource(iframe);
      //           break;
      //         case "air_iframe":
      //           iframeSource(iframe);
      //           break;
      //         default:
      //           console.log("No iframe loaded");
      //       }
      //     });
      //   }
      // );

      iframes.forEach((item) => {
        const contentheight = mainContent.clientHeight;
        item.style.minHeight = `${contentheight}px`;
      });
    }
  };

  // single wpdmpro, admin subpages, e-newsletter archive
  const fnPageWPDM = (body) => {
    const adminSubPage = document.querySelector(".admin_sub_page");

    if (body.classList.contains("single-wpdmpro")) {
      const w3edens = [...adminSubPage.querySelectorAll(".w3eden, .card-body")];

      w3edens.forEach((w3eden) => {
        const cards = w3eden.querySelectorAll(".card");
        const listGroups = w3eden.querySelectorAll(".list-group-item");
        const attachTitles = w3eden.querySelectorAll("h3");

        if (
          cards.length > 0 ||
          attachTitles.length > 0 ||
          listGroups.length > 0
        ) {
          cards.forEach((card) => {
            if (card.textContent == null || card.innerText == "") {
              card.remove();
            }
          });

          attachTitles.forEach((title) => {
            if (title.textContent == "Attached Files") {
              const icon = document.createElement("i");
              icon.setAttribute("class", "fas fa-paperclip");
              title.prepend(icon);
            }
          });

          listGroups.forEach((list) => {
            if (
              list.textContent.includes("Create Date") ||
              list.textContent.includes("Last Updated") ||
              list.textContent.includes("Download")
            ) {
              list.remove();
            }
          });
        }
      });
    }

    if (body.classList.contains("parent-pageid-463")) {
      const cardDeckWrapper = adminSubPage.querySelector(".card-deck-wrapper");

      if (cardDeckWrapper !== null) {
        const cardDecks = [...cardDeckWrapper.querySelectorAll(".card-deck")];
        const sideMenu = document.createElement("div");
        sideMenu.id = "md_scrollspy";
        sideMenu.className = "md_scrollspy";

        const mediaBody = cardDecks.map((deck) =>
          deck.querySelectorAll(".media-body")
        );
        const cardHeaders = cardDecks.map((deck) =>
          deck.querySelectorAll(".card-header")
        );

        cardDecks.forEach((deck) => {
          const pagi = deck.querySelector(".wpdm-pagination");

          if (pagi !== null && pagi.children.length < 1) {
            pagi.remove();
          }
        });

        if (mediaBody.length > 0) {
          mediaBody.forEach(([...items]) => {
            items.forEach((item) => {
              const statsDownload = item.innerHTML.indexOf(
                '<i class="ml-3 far fa-arrow-alt-circle-down mr-1"></i>'
              );

              if (statsDownload > -1)
                item.innerHTML = item.innerHTML.slice(0, statsDownload);
            });
          });
        }

        if (cardHeaders.length > 0) {
          cardHeaders.forEach(([...items]) => {
            items.forEach((item, i) => {
              const titleTxt = item.textContent;

              if (titleTxt !== "") {
                const anchor = document.createElement("a");
                const oldtxt = titleTxt
                  .replace(/[^a-zA-Z ]/g, "")
                  .toLocaleLowerCase();
                const hash = oldtxt.replace(/ /gm, "-");

                anchor.textContent = titleTxt;
                anchor.className = "scrollspy_link";
                anchor.href = `#${hash}`;
                item.setAttribute("id", hash);

                if (sideMenu.children.length > 0) {
                  const allHash = [...sideMenu.querySelectorAll("a")];
                  const hasHash = allHash.findIndex(
                    (hash) => hash.textContent === titleTxt
                  );

                  if (hasHash > -1) {
                    anchor.setAttribute("href", `#${hash}-${i}`);
                    item.setAttribute("id", `${hash}-${i}`);
                    sideMenu.appendChild(anchor);
                  } else {
                    sideMenu.appendChild(anchor);
                  }
                } else {
                  sideMenu.appendChild(anchor);
                }
              }
            });
          });
        }

        if (sideMenu.children.length > 0) {
          adminSubPage.prepend(sideMenu);

          const mdScrollspy = () => {
            // Variables
            const $window = $(window);
            const $links = $("#md_scrollspy a");
            const $sections = getSections($links);
            const $root = $("html, body");
            const $hashLinks = $('a[href^="#"]:not([href="#"])');

            // Functions
            function getSections($links) {
              const data = $(
                $links
                  .map((i, el) => $(el).attr("href"))
                  .toArray()
                  .filter((href) => href.charAt(0) === "#")
                  .join(",")
              );
              return data;
              // console.log(data)
            }

            function onScrollHandler(e) {
              activateLink($sections, $links);
              // console.log(activateLink($sections, $links))
            }

            function activateLink($sections, $links) {
              const yPosition = $window.scrollTop() + 250;

              for (let i = $sections.length - 1; i >= 0; i -= 1) {
                const $section = $sections.eq(i);
                const scrollval = $section.offset().top;

                if (yPosition >= scrollval) {
                  return $links
                    .removeClass("active")
                    .filter(`[href="#${$section.attr("id")}"]`)
                    .addClass("active");
                }
              }
            }

            function onClickHandler(e) {
              e.preventDefault();
              const href = $.attr(e.target, "href");
              $hashLinks.removeClass("active");
              $(`#md_scrollspy a[href="${href}"`).addClass("active");

              $root.animate(
                {
                  scrollTop: $(href).offset().top + -165,
                },
                500
              );

              return false;
            }

            // Events
            $window.on("scroll", onScrollHandler);
            $hashLinks.on("click", onClickHandler);
            activateLink($sections, $links);
          };
          mdScrollspy();
        } else {
          sideMenu.remove();
        }
      } else {
        adminSubPage
          .querySelector(".row")
          .setAttribute(
            "style",
            "flex-basis: 100%; width: 100%; justify-content: center; max-width: 100%; margin: 0;"
          );
      }
    }

    if (body.classList.contains("page-enewsletter-archive")) {
      const w3edens = [...adminSubPage.querySelectorAll(".card-body")];

      w3edens.forEach((card) => {
        const mailLinks = card.querySelectorAll("ul a");
        mailLinks.forEach((link) => {
          link.setAttribute("target", "_blank");
        });
      });
    }
  };

  // document search
  const fnWellFormCategorySelect = () => {
    const wellForm = document.querySelector("form.well");
    if (wellForm) {
      const formCategory = wellForm.querySelector("#category");
      const publishDate = wellForm.querySelector("#publish_date");
      const updateDate = wellForm.querySelector("#update_date");
      const viewCount = wellForm.querySelector("#view_count");
      const downloadCount = wellForm.querySelector("#download_count");
      const packageSize = wellForm.querySelector("#package_size");
      const orderBy = wellForm.querySelector("#order_by");
      const search = wellForm.querySelector("#s");
      const btns = wellForm.querySelectorAll("button");
      const btnWrapper = document.createElement("div");

      btnWrapper.className = "r__btns";
      wellForm.appendChild(btnWrapper);

      btns.forEach((btn) => {
        if (btn.hasAttribute("type")) {
          btnWrapper.appendChild(btn);
        }
      });

      if (
        publishDate ||
        updateDate ||
        viewCount ||
        downloadCount ||
        packageSize ||
        orderBy ||
        search ||
        formCategory
      ) {
        publishDate.closest(".row").style.display = "none";
        updateDate.closest(".row").style.display = "none";
        viewCount.closest(".row").style.display = "none";
        downloadCount.closest(".row").style.display = "none";
        packageSize.closest(".row").style.display = "none";
        orderBy.closest(".row").classList.add("r__orderBy");
        search.closest(".row").classList.add("r__search");
        formCategory.closest(".row").classList.add("r__category");
      }
    }
  };

  const handleCatTree = () => {
    if (location.search.indexOf("?adb_page=edit-package") === -1) return;

    const catTree = document.querySelector("#wpdmcat-tree");

    if (catTree === null) return;

    const catTreeLis = [...catTree.querySelectorAll("li")];
    const activeCat = catTreeLis.findIndex(
      (li) => li.querySelector("input").checked
    );

    if (activeCat > -1) return;

    const otherLi = catTreeLis.find(
      (li) => li.textContent.indexOf("Other") > -1
    );
    const liInput = otherLi.querySelector("input");
    liInput.checked = true;
  };

  document.addEventListener("DOMContentLoaded", function () {
    const body = document.body;
    const url = location.search;

    if (url !== "") {
      if (location.search.indexOf("?search=") > -1) {
        const param = url.replace("?search=", "");
        document.querySelector("#advpack_filter").style.display = "none";
        location.replace(
          `${location.pathname}/?skw=${param}&orderby=date&order=desc`
        );
      }
    }

    const btns = document.querySelectorAll(".input-group-btn");
    if (btns.length > 0) {
      btns.forEach((btn) => {
        if (btn.innerHTML.includes("fa-search") && btn) {
          btn.classList.add("fix_search");
        }
      });
    }

    fnAuthorDashboard(body);
    fnEditProfileForm();
    fnIIIframe();
    fnPageWPDM(body);
    fnWellFormCategorySelect();
    handleCatTree();
  });
})(jQuery);

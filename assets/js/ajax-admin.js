(function ($) {

  const fnCreatePostBackEndAjax = () => {
    const pageURL = location.pathname;
    const taxGroup = `${pageURL}?taxonomy=group`;

    if (location.href.indexOf(`${taxGroup}`) > -1) {

      const editTag = document.querySelector('#edittag');

      if (editTag) {

        let clicked = false;
        const termName = editTag.querySelector('#name');
        const termID = editTag.querySelector('[name=tag_ID]');
        const termIDValue = termID.value;
        const termNameValue = termName.value;

        console.log(termIDValue);
        console.log(termNameValue);

        editTag.addEventListener('submit', (e) => {
          // e.preventDefault();

          if (!clicked) {
            clicked = true;

            const groupUsers = document.querySelector('[data-name=group_users]');
            const memberNames = groupUsers.querySelectorAll('[data-name=member_name] option');

            function fnGetMembersID() {
              let ids = new Array();

              Array.from(memberNames).forEach(id => {
                ids.push(id.value)
              })
              return ids.toString()
            }


            function fnGetMembersIDWithMinus() {
              let ids = new Array();

              Array.from(memberNames).forEach(id => {
                ids.push(`-${id.value}`)
              })
              return ids.toString()
            }


            function fnGetMembersName() {
              let memberArr = new Array();

              if (memberNames.length > 0) {
                for (let i = 0; i < memberNames.length; i++) {
                  const user = memberNames[i].textContent
                  const userOpenIndex = user.indexOf('(');
                  const userInitial = user.slice(0, parseInt(userOpenIndex));
                  const userSpaceIndex = userInitial.lastIndexOf(' ');
                  const userLoginValue = userInitial.slice(0, parseInt(userSpaceIndex));

                  if (!memberArr.includes(`${userLoginValue}`)) {
                    memberArr.push(userLoginValue);
                  }
                }

              }
              return memberArr;
            }

            // console.log(fnGetMembersName())


            if (termIDValue && termNameValue && fnGetMembersID() != undefined && fnGetMembersIDWithMinus() != undefined && fnGetMembersName() != undefined) {
              console.log(fnGetMembersID())
              console.log(fnGetMembersIDWithMinus())
              console.log(fnGetMembersName())
            }

            $.ajax({
              url: ajaxsharedpost.ajaxurl,
              type: 'POST',
              data: {
                action: 'my_ajax_shared_post',
                user_id: fnGetMembersName(),
                term_id: termIDValue,
                term_name: termNameValue,
                post_idin: fnGetMembersID(),
                post_idex: fnGetMembersIDWithMinus(),
              },
              beforeSend: function (html) {},
              success: function (html) {},
            });
          }
        });

        const fnTermPHP = () => {
          let arrayAuthors = new Array();
          const adminTermAuthors = document.querySelectorAll('.admin_terms [data-term-author]')
          const noticeDiv = document.createElement('div')
          noticeDiv.id = 'member_warning'
          noticeDiv.classList.add('warn')
          noticeDiv.style.display = 'none'
          noticeDiv.textContent = 'One of the members you selected is assigned in other group.'
          document.querySelector('.edit-tag-actions').appendChild(noticeDiv)

          if (adminTermAuthors.length > 0) {
            for (let a = 0; a < adminTermAuthors.length; a++) {
              const author = adminTermAuthors[a].getAttribute('data-term-author')

              if (!arrayAuthors.includes(author)) {
                arrayAuthors.push(author)
              }
            }
            console.log(arrayAuthors)


            setTimeout(() => {
              let arrayNames = new Array();
              const renderedNamesInitial = document.querySelectorAll('.acf-table .select2-selection__rendered')

              if (renderedNamesInitial.length > 0) {
                renderedNamesInitial.forEach(name => {
                  const nameValue = name.getAttribute('title')
                  if (!arrayNames.includes(nameValue)) {
                    arrayNames.push(nameValue)
                  }
                })
              }
              console.log(arrayNames)

              const dontIncludeAuthors = arrayAuthors.filter(author => {
                if (!arrayNames.includes(author)) return author
              })
              console.log(dontIncludeAuthors)

              if (dontIncludeAuthors.length > 0) {
                let exitCheck = setInterval(function () {
                  let able = false;
                  const renderedNames = document.querySelectorAll('.acf-table .select2-selection__rendered')

                  Array.from(renderedNames).forEach(name => {
                    dontIncludeAuthors.find(author => {
                      if (author == name.getAttribute('title')) {

                        if (!document.querySelector('.edit-tag-actions input').disabled && (!able)) {
                          document.querySelector('.edit-tag-actions input').disabled = true
                          noticeDiv.style.display = 'block'
                          able = true;
                        }
                      } else {
                        if (document.querySelector('.edit-tag-actions input').disabled && (!able)) {
                          document.querySelector('.edit-tag-actions input').disabled = false
                          noticeDiv.style.display = 'none'
                        }
                      }
                    })
                  })
                }, 50)
              }

            }, 2000)
          }
        }

        window.addEventListener('load', fnTermPHP)

      }
    }
  }

  document.addEventListener('DOMContentLoaded', fnCreatePostBackEndAjax)

})(jQuery);